<?php
declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use App\Infrastructure\Persistence\User\InMemoryUserRepository;
use DI\ContainerBuilder;
use App\Domain\GeodeticPoint\GeodeticPointRepository;
use App\Domain\GeodeticPointMission\GeodeticPointMissionRepository;
use App\Domain\GeodeticPointTask\GeodeticPointTaskRepository;
use App\Domain\Synchronization\SynchronizationRepository;
use App\Domain\Image\ImageRepository;
use App\Domain\Mission\MissionRepository;
use App\Domain\Note\NoteRepository;
use App\Domain\Task\TaskRepository;
use App\Domain\User\UserRepository;
use App\Domain\UserGroup\UserGroupRepository;
use App\Domain\GeodeticPointTaskConflict\GeodeticPointTaskConflictRepository;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        // UserRepository::class => \DI\autowire(InMemoryUserRepository::class),
        
        GeodeticPointRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new GeodeticPointRepository($entityManager, $logger);
        },
        
        GeodeticPointTaskRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new GeodeticPointTaskRepository($entityManager, $logger);
        },

        GeodeticPointMissionRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new GeodeticPointMissionRepository($entityManager, $logger);
        },

        SynchronizationRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new SynchronizationRepository($entityManager, $logger);
        },

        ImageRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new ImageRepository($entityManager, $logger);
        },

        MissionRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new MissionRepository($entityManager, $logger);
        },

        NoteRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new NoteRepository($entityManager, $logger);
        },

        TaskRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new TaskRepository($entityManager, $logger);
        },

        UserRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new UserRepository($entityManager, $logger);
        },

        UserGroupRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new UserGroupRepository($entityManager, $logger);
        },

        GeodeticPointTaskConflictRepository::class => function (ContainerInterface $container) {
            $logger = $container->get(LoggerInterface::class);
            $entityManager = $container->get(EntityManagerInterface::class);
            return new GeodeticPointTaskConflictRepository($entityManager, $logger);
        },
    ]);
};
