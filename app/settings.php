<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

// Load .env config
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'production' => false,
            'displayErrorDetails' => true, // Should be set to false in production
            'logger' => [
                'name' => 'slim-app',
                'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                'level' => Logger::DEBUG,
            ],
            'doctrine' => [
                // if true, metadata caching is forcefully disabled
                'dev_mode' => true,
                
                // path where the compiled metadata info will be cached
                // make sure the path exists and it is writable
                'cache_dir' => __DIR__ . '/../var/doctrine',
                
                // you should add any other path containing annotated entity classes
                'metadata_dirs' => [__DIR__ . '/../src/Domain'],

                'connection' => [
                    'driver' => 'pdo_mysql',
                    'host' => 'localhost',
                    'port' => $_ENV["DB_PORT"],
                    'dbname' => 'egeodet',
                    'user' => $_ENV["DB_USER"],
                    'password' => $_ENV["DB_PASSWORD"],
                    'charset' => 'UTF8'
                    ]
                ],
                'twig' => [
                'paths' => [
                    __DIR__ . '/../templates',
                ],
                // Twig environment settings
                'options' => [
                    // Should be set to true in production
                    'cache_enabled' => false,
                    'cache_path' => __DIR__ . '/../tmp/twig'
                ],
            ],
            'uploadDirectory' => __DIR__ . '/../uploads',
        ],
    ]);
};
