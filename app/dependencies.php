<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Codeliner\ArrayReader\ArrayReader; 

// Include ORM entities
if (file_exists(__DIR__ . "\..\src\Domain\DoctrineMigrationVersion.php"))
    require_once(__DIR__ . "\..\src\Domain\DoctrineMigrationVersion.php");
if (file_exists(__DIR__ . "\..\src\Domain\GeodeticPointMission.php"))
    require_once(__DIR__ . "\..\src\Domain\GeodeticPointMission.php");
if (file_exists(__DIR__ . "\..\src\Domain\GeodeticPoint.php"))
    require_once(__DIR__ . "\..\src\Domain\GeodeticPoint.php");
if (file_exists(__DIR__ . "\..\src\Domain\GeodeticPointTask.php"))
    require_once(__DIR__ . "\..\src\Domain\GeodeticPointTask.php");
if (file_exists(__DIR__ . "\..\src\Domain\Image.php"))
    require_once(__DIR__ . "\..\src\Domain\Image.php");
if (file_exists(__DIR__ . "\..\src\Domain\Mission.php"))
    require_once(__DIR__ . "\..\src\Domain\Mission.php");
if (file_exists(__DIR__ . "\..\src\Domain\Note.php"))
    require_once(__DIR__ . "\..\src\Domain\Note.php");
if (file_exists(__DIR__ . "\..\src\Domain\Task.php"))
    require_once(__DIR__ . "\..\src\Domain\Task.php");
if (file_exists(__DIR__ . "\..\src\Domain\User.php"))
    require_once(__DIR__ . "\..\src\Domain\User.php");
if (file_exists(__DIR__ . "\..\src\Domain\Group.php"))
    require_once(__DIR__ . "\..\src\Domain\Group.php");
if (file_exists(__DIR__ . "\..\src\Domain\UserGroup.php"))
    require_once(__DIR__ . "\..\src\Domain\UserGroup.php");
if (file_exists(__DIR__ . "\..\src\Domain\Synchronization.php"))
    require_once(__DIR__ . "\..\src\Domain\Synchronization.php");
if (file_exists(__DIR__ . "\..\src\Domain\GeodeticPointTaskConflict.php"))
    require_once(__DIR__ . "\..\src\Domain\GeodeticPointTaskConflict.php");

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },

        EntityManagerInterface::class => function (ContainerInterface $c): EntityManager {
            $doctrineSettings = $c->get('settings')['doctrine'];
    
            // $config = Setup::createAnnotationMetadataConfiguration(
            //     $doctrineSettings['metadata_dirs'],
            //     $doctrineSettings['dev_mode'],
            // );

            $config = Setup::createXMLMetadataConfiguration(
                $doctrineSettings['metadata_dirs'],
                $doctrineSettings['dev_mode'],
            );
    
            // $config->setMetadataDriverImpl(
            //     new AnnotationDriver(
            //         new AnnotationReader,
            //         $doctrineSettings['metadata_dirs']
            //     )
            // );
    
            $config->setMetadataCacheImpl(
                new FilesystemCache($doctrineSettings['cache_dir'])
            );
    
            return EntityManager::create($doctrineSettings['connection'], $config);
        },

        Twig::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');
            $twigSettings = $settings['twig'];
    
            $options = $twigSettings['options'];
            $options['cache'] = $options['cache_enabled'] ? $options['cache_path'] : false;
    
            $twig = Twig::create($twigSettings['paths'], $options);
    
            // Add extension here
            // ...
            
            return $twig;
        },

        TwigMiddleware::class => function (ContainerInterface $container) {
            return TwigMiddleware::createFromContainer(
                $container->get(App::class),
                Twig::class
            );
        },

        ArrayReader::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');
            return new ArrayReader([
                'uploadDirectory' => $settings['uploadDirectory'],
            ]);
        },
    ]);
};
