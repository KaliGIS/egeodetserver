<?php
declare(strict_types=1);

use App\Application\Actions\Synchronization\SynchronizeSynchronizationAction;
use App\Application\Actions\Synchronization\GetSynchronizationAction;
use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\GetUserAction;
use App\Application\Actions\User\SynchronizeUserAction;
use App\Application\Actions\Mission\GetMissionAction;
use App\Application\Actions\Mission\ListMissionsAction;
use App\Application\Actions\Mission\SynchronizeMissionAction;
use App\Application\Actions\GeodeticPoint\ListGeodeticPointsAction;
use App\Application\Actions\GeodeticPoint\InsertGeodeticPointAction;
use App\Application\Actions\GeodeticPoint\SynchronizeGeodeticPointAction;
use App\Application\Actions\GeodeticPoint\UpdateGeodeticPointAction;
use App\Application\Actions\GeodeticPointMission\ListGeodeticPointMissionsAction;
use App\Application\Actions\GeodeticPointMission\InsertGeodeticPointMissionAction;
use App\Application\Actions\GeodeticPointMission\SynchronizeGeodeticPointMissionAction;
use App\Application\Actions\GeodeticPointTask\ListGeodeticPointTasksAction;
use App\Application\Actions\GeodeticPointTask\InsertGeodeticPointTaskAction;
use App\Application\Actions\GeodeticPointTask\SynchronizeGeodeticPointTaskAction;
use App\Application\Actions\Note\SynchronizeNoteAction;
use App\Application\Actions\Note\ListNotesAction;
use App\Application\Actions\Image\ListImagesAction;
use App\Application\Actions\Image\UploadImagesAction;
use App\Application\Actions\Task\ListTasksAction;
use App\Application\Actions\UserGroup\ListUserGroupsAction;
use App\Application\Actions\Twig\HelloAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world, man!');
        return $response;
    });

    $app->get('/hello', HelloAction::class);

    $app->group('/api/synchronizations', function (Group $group) {
        $group->post('/sync', SynchronizeSynchronizationAction::class);
        $group->get('/{id}', GetSynchronizationAction::class);
    });

    $app->group('/api/users', function (Group $group) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', GetUserAction::class);
        $group->post('/sync', SynchronizeUserAction::class);
    });

    $app->group('/api/missions', function (Group $group) {
        $group->get('', ListMissionsAction::class);
        $group->get('/{id}', GetMissionAction::class);
    });

    $app->group('/api/geodeticpoints', function (Group $group) {
        $group->get('', ListGeodeticPointsAction::class);
        $group->post('', InsertGeodeticPointAction::class);
        $group->post('/sync', SynchronizeGeodeticPointAction::class);
        $group->put('/{id}', UpdateGeodeticPointAction::class);
    }); 

    $app->group('/api/geodeticpointmissions', function (Group $group) {
        $group->get('', ListGeodeticPointMissionsAction::class);
        $group->post('', InsertGeodeticPointMissionAction::class);
        $group->post('/sync', SynchronizeGeodeticPointMissionAction::class);
    }); 

    $app->group('/api/geodeticpointtasks', function (Group $group) {
        $group->get('', ListGeodeticPointTasksAction::class);
        $group->post('', InsertGeodeticPointTaskAction::class);
        $group->post('/sync', SynchronizeGeodeticPointTaskAction::class);
    });

    $app->group('/api/notes', function (Group $group) {
        $group->get('', ListNotesAction::class);
        $group->post('/sync', SynchronizeNoteAction::class);
    });

    $app->group('/api/images', function (Group $group) {
        $group->get('', ListImagesAction::class);
        $group->post('/sync', UploadImagesAction::class);
    });

    $app->group('/api/tasks', function (Group $group) {
        $group->get('', ListTasksAction::class);
    });

    $app->group('/api/usergroups', function (Group $group) {
        $group->get('', ListUserGroupsAction::class);
    });
};
