<?php
declare(strict_types=1);

use App\Application\Middleware\SessionMiddleware;
use App\Application\Middleware\JsonBodyParserMiddleware;
use Tuupola\Middleware\JwtAuthentication;
use Slim\App;

return function (App $app) {
    $app->add(SessionMiddleware::class);
    $app->add(JsonBodyParserMiddleware::class);
    $app->add(new Tuupola\Middleware\JwtAuthentication([
        "secure" => false,
        "path" => $_ENV['PATH_AUTH_NEEDED'],
        "secret" => $_ENV['API_SECRET'],
        "algorithm" => ["HS256", "HS384"]
    ]));
};
