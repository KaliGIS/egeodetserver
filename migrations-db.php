<?php

return [
    'dbname' => 'egeodet',
    'user' => $_ENV["DB_USER"],
    'password' => $_ENV["DB_PASSWORD"],
    'host' => 'localhost',
    'port' => $_ENV["DB_PORT"],
    'driver' => 'pdo_mysql',
];