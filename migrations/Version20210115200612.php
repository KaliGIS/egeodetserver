<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210115200612 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adds password field to users table';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `users` ADD COLUMN IF NOT EXISTS (`password` TEXT NOT NULL)');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `users` DROP COLUMN IF EXISTS `password`');

    }
}
