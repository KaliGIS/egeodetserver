<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210515170033 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Replace procedure selectOrInsertMission';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE OR REPLACE PROCEDURE selectOrInsertMission(IN name TEXT, IN year INT, IN group_id INT, IN lifecycle_phase INT, OUT mission_id BIGINT)
            BEGIN
                DECLARE missionId BIGINT;
                SELECT `id` INTO missionId FROM `missions` WHERE `missions`.`name` = name AND `missions`.`year` = year LIMIT 1; 
                IF (missionId IS NULL) THEN 
                    INSERT INTO `missions` (`name`, `year`, `group_id`, `lifecycle_phase`) VALUES  (name, year, group_id, lifecycle_phase);
                    SET mission_id = LAST_INSERT_ID();
                ELSE
                    SET mission_id = missionId;
                END IF;
            END;
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            CREATE OR REPLACE PROCEDURE selectOrInsertMission(IN name TEXT, IN year INT, IN group_id INT, OUT mission_id BIGINT)
            BEGIN
                DECLARE missionId BIGINT;
                SELECT `id` INTO missionId FROM `missions` WHERE `missions`.`name` = name AND `missions`.`year` = year LIMIT 1; 
                IF (missionId IS NULL) THEN 
                    INSERT INTO `missions` (`name`, `year`, `group_id`) VALUES  (name, year, group_id);
                    SET mission_id = LAST_INSERT_ID();
                ELSE
                    SET mission_id = missionId;
                END IF;
            END;
        ');
    }
}
