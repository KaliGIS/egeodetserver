<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201203121812 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adds new "synchronizations" table to database';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE `synchronizations` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `sync_key` TEXT, `user_id` INTEGER, `start_date` TIMESTAMP NOT NULL, `end_date` TIMESTAMP, `status` INTEGER COMMENT "0 - synced, 1 - syncing, 2 - synced with error") ENGINE = InnoDB');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE `synchronizations`');

    }
}
