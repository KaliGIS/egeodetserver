<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210320114841 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `geodetic_point_tasks` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE SET NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `geodetic_point_tasks` DROP FOREIGN KEY IF EXISTS `geodetic_point_tasks_ibfk_5`');
    }
}
