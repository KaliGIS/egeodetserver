<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210121192644 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Creates table user_groups and adds column group_id to users, geodetic_point_missions and geodetic_point_tasks';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE `groups` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `name` TEXT, `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user_groups` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `user_id` BIGINT, `group_id` BIGINT, `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE CASCADE, FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `geodetic_point_tasks` ADD COLUMN (`group_id` BIGINT, FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE SET NULL)');
        $this->addSql('ALTER TABLE `geodetic_point_missions` ADD COLUMN (`group_id` BIGINT, FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE SET NULL)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `geodetic_point_tasks` DROP COLUMN `group_id`');
        $this->addSql('ALTER TABLE `geodetic_point_missions` DROP COLUMN `group_id`');
        $this->addSql('DROP TABLE `groups`');
        $this->addSql('DROP TABLE `user_groups`');
    }
}
