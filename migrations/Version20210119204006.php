<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210119204006 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Modify on insert start_date and on update end_date in synchronizations';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `synchronizations` MODIFY COLUMN `start_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE `synchronizations` MODIFY COLUMN `end_date` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `synchronizations` MODIFY COLUMN `start_date` TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE `synchronizations` MODIFY COLUMN `end_date` TIMESTAMP');
    }
}
