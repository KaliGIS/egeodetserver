<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210309124249 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add table geodetic_point_task_conflicts';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE `geodetic_point_task_conflicts` (
            `id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL,
            `geodetic_point_task_id` BIGINT NOT NULL, 
            `mission_id` BIGINT NOT NULL,
            `geodetic_point_id` BIGINT NOT NULL, 
            `task_id` BIGINT NOT NULL, 
            `user_id` BIGINT, 
            `group_id` BIGINT,
            `is_mandatory` INTEGER NOT NULL, 
            `is_done` INTEGER NOT NULL, 
            `done_date` TIMESTAMP NULL, 
            `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
            `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            FOREIGN KEY (`geodetic_point_task_id`) REFERENCES `geodetic_point_tasks`(`id`) ON UPDATE CASCADE ON DELETE CASCADE ,
            FOREIGN KEY (`mission_id`) REFERENCES `missions`(`id`) ON UPDATE CASCADE ON DELETE CASCADE ,
            FOREIGN KEY (`geodetic_point_id`) REFERENCES `geodetic_points`(`id`) ON UPDATE CASCADE ON DELETE CASCADE , 
            FOREIGN KEY (`task_id`) REFERENCES `tasks`(`id`) ON UPDATE CASCADE ON DELETE CASCADE ,
            FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE SET NULL
            ) ENGINE = InnoDB ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE `geodetic_point_task_conflicts`');
    }
}
