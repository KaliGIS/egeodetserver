<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201225185229 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Alter field done_date of geodetic_point_tasks table from text to timestamp type';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `geodetic_point_tasks` MODIFY `done_date` TIMESTAMP NULL');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `geodetic_point_tasks` MODIFY `done_date` TEXT NULL');

    }
}
