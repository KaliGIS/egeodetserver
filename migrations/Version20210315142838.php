<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210315142838 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add field group_id to table missions';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `missions` ADD COLUMN (`group_id` BIGINT, FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE SET NULL)');
    }

    public function down(Schema $schema) : void
    { 
        $this->addSql('ALTER TABLE `missions` DROP COLUMN `group_id`');
    }
}
