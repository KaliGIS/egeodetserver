<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201114104821 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'DDL migration';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE `geodetic_points` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `label` TEXT, `type` TEXT, `last_maintenance` TIMESTAMP NULL, `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `missions` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `name` TEXT, `year` INTEGER NOT NULL, `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `users` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `name` TEXT, `role` TEXT, `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `tasks` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `name` TEXT, `type` TEXT, `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `images` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `name` TEXT, `path` TEXT, `geodetic_point_id` BIGINT NOT NULL, `mission_id` BIGINT NOT NULL, `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, FOREIGN KEY(geodetic_point_id) REFERENCES geodetic_points(id) ON UPDATE CASCADE ON DELETE CASCADE, FOREIGN KEY(mission_id) REFERENCES missions(id) ON UPDATE CASCADE ON DELETE CASCADE ) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `notes` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `note` TEXT, `user_id` BIGINT NOT NULL, `geodetic_point_id` BIGINT NOT NULL, `mission_id` BIGINT NOT NULL, `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, FOREIGN KEY (`mission_id`) REFERENCES `missions` (`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY (`geodetic_point_id`) REFERENCES `geodetic_points` (`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE CASCADE ) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `geodetic_point_missions` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `mission_id` BIGINT NOT NULL, `geodetic_point_id` BIGINT NOT NULL, `user_id` BIGINT, `status` INTEGER, `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, FOREIGN KEY(`mission_id`) REFERENCES `missions`(`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`geodetic_point_id`) REFERENCES `geodetic_points`(`id`) ON UPDATE CASCADE ON DELETE CASCADE ) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `geodetic_point_tasks` (`id` BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL, `mission_id` BIGINT NOT NULL, `geodetic_point_id` BIGINT NOT NULL, `task_id` BIGINT NOT NULL, `user_id` BIGINT, `is_mandatory` INTEGER NOT NULL, `is_done` INTEGER NOT NULL, `done_date` TEXT, `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, FOREIGN KEY(`mission_id`) REFERENCES `missions`(`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`geodetic_point_id`) REFERENCES `geodetic_points`(`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`task_id`) REFERENCES `tasks`(`id`) ON UPDATE CASCADE ON DELETE CASCADE ) ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE `geodetic_point_missions`');
        $this->addSql('DROP TABLE `geodetic_point_tasks`');
        $this->addSql('DROP TABLE `geodetic_points`');
        $this->addSql('DROP TABLE `images`');
        $this->addSql('DROP TABLE `missions`');
        $this->addSql('DROP TABLE `notes`');
        $this->addSql('DROP TABLE `tasks`');
        $this->addSql('DROP TABLE `users`');
    }
}
