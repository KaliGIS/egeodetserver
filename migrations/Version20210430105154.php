<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210430105154 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Data lifecycle phases setup';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `missions` ADD COLUMN IF NOT EXISTS `lifecycle_phase` INTEGER NOT NULL DEFAULT 1 COMMENT '0 - v príprave, 1 - v spracovaní, 2 - po spracovaní' AFTER `year`");
        $this->addSql("ALTER TABLE `missions` ADD CONSTRAINT `lifecycle_phase_check_constraint` CHECK (`lifecycle_phase` IN (0, 1, 2))");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `missions` DROP CONSTRAINT `lifecycle_phase_check_constraint`');
        $this->addSql('ALTER TABLE `missions` DROP COLUMN IF EXISTS `lifecycle_phase`');
    }
}
