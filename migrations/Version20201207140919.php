<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201207140919 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Alters end_date column of the table synchronizations to be nullable';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `synchronizations` MODIFY `end_date` TIMESTAMP NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `synchronizations` MODIFY `end_date` TIMESTAMP');
    }
}
