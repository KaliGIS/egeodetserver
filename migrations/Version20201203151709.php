<?php

declare(strict_types=1);

namespace EGeodet\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201203151709 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Alters users table by adding new column "email"';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `users` ADD COLUMN IF NOT EXISTS (`email` TEXT)');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `users` DROP COLUMN IF EXISTS `email`');

    }
}
