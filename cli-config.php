<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use DI\ContainerBuilder;

// Instantiate PHP-DI ContainerBuilder
$containerBuilder = new ContainerBuilder();

// Set up settings
$settings = require 'app/settings.php';
$settings($containerBuilder);

// Set up dependencies
$dependencies = require 'app/dependencies.php';
$dependencies($containerBuilder);

// Build PHP-DI Container instance
$container = $containerBuilder->build();

// replace with mechanism to retrieve EntityManager in your app
$entityManager = $container->get(EntityManagerInterface::class);

return ConsoleRunner::createHelperSet($entityManager);