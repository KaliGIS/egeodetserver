<?php
declare(strict_types=1);

namespace App\Domain\Note;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Exception;
use DateTime;
use App\Domain\Validator\DateTimeValidator;


class NoteRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    private $defaultNoteData = array(
        "note" => null,
        "userId" => null,
        "geodeticPointId" => null,
        "missionId" => null,
        "created" => null,
        "modified" => null
    );

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return Note
     */
    public function synchronizeNote(array $noteData) : \Note {
        // $this->logger->info("Changed data: " . json_encode($noteData));
        $id = empty($noteData["id"]) ? null : $noteData["id"];
        $userId = empty($noteData["userId"]) ? null : $noteData["userId"];
        $missionId = empty($noteData["missionId"]) ? null : $noteData["missionId"];
        $noteEntity = $this->em->getRepository('Note')->findOneBy(array("id" => $id));
        $this->logger->info('Incoming note id: ' . $id);

        if (!empty($noteEntity) 
            && $noteEntity->getUserId() == $userId
            && $noteEntity->getMissionId() == $missionId
        ) {
            // update existing note
            $noteEntity = $this->updateNote((int) $noteEntity->getId(), $noteData, $noteEntity);
        } else {
            // insert new note
            $noteEntity = $this->insertNote($noteData);
        }

        $this->logger->info("Note entity to return: " . json_encode($noteEntity));
        return $noteEntity;
    }

    /**
     * @return Note
     */
    public function insertNote(array $noteData): \Note
    {
        
        $missionId = $this->extractMissionId($noteData);
        $missionEntity = $this->em->getRepository('Mission')->findOneBy(array("id" => $missionId));
        if (empty($missionEntity)) {
            throw new Exception("Mission entity does not exist for id " . $missionId);
        }

        $geodeticPointId = $this->extractGeodeticPointId($noteData);
        $userId = $this->extractUserId($noteData);

        $harmonizedData = $this->harmonizeData($noteData);
        
        $newNote = new \Note();
        $newNote->setNote($harmonizedData["note"]);
        $newNote->setUserId($userId);
        $newNote->setGeodeticPointId($geodeticPointId);
        $newNote->setMissionId($missionId);
        $newNote->setCreated($harmonizedData["created"]);
        $newNote->setModified($harmonizedData["modified"]);
        $newNote->setMission($missionEntity);
        
        $this->logger->info('I am here with final note data: ' . json_encode($newNote));

        $this->em->persist($newNote);
        $this->em->flush();

        return $newNote;
    }

    /**
     * @return Note
     */
    public function updateNote(int $id, array $noteData, \Note $noteEntity = null): \Note {
        // $this->logger->info("Note id in in the beginning of updateNote: " . $noteEntity->getId());
        $missionId = $this->extractMissionId($noteData);
        $geodeticPointId = $this->extractGeodeticPointId($noteData);
        $userId = $this->extractUserId($noteData);
        
        $harmonizedData = $this->harmonizeData($noteData);

        if (empty($noteEntity)) {
            // $this->logger->info("Note entity is empty. Getting by id.");
            $noteEntity = $this->em->getRepository('Note')->findOneBy(array("id" => $id));
        }
        
        $noteEntity->setNote($harmonizedData["note"]);
        $noteEntity->setUserId($userId);
        $noteEntity->setGeodeticPointId($geodeticPointId);
        $noteEntity->setMissionId($missionId);
        $noteEntity->setCreated($harmonizedData["created"]);
        $noteEntity->setModified($harmonizedData["modified"]);

        // $this->logger->info("Note id before flush: " . $noteEntity->getId());
        $this->em->flush();

        // $this->logger->info("Note id in to return note: " . $noteEntity->getId());
        return $noteEntity;
    }

    /**
     * Transforms data to be insertable
     * @return array
     */
    public function harmonizeData(array $noteData): array {
        if (array_key_exists("created", $noteData) && DateTimeValidator::isValid($noteData["created"])) {
            $noteData["created"] = new DateTime($noteData["created"]);
        } else {
            unset($noteData["created"]);
        }
        if (array_key_exists("modified", $noteData) && DateTimeValidator::isValid($noteData["modified"])) {
            $noteData["modified"] = new DateTime($noteData["modified"]);
        } else {
            unset($noteData["modified"]);
        }

        $harmonizedData = array_replace($this->defaultNoteData, $noteData);
        return $harmonizedData;
    }

    /**
     * Extracts mission id from noteData if exists
     */
    public function extractMissionId($noteData): int {
        if (!array_key_exists("missionId", $noteData) || empty($noteData["missionId"])) {
            throw new Exception("No mission data provided in the request body");
        }

        return (int) $noteData["missionId"];
    }

    /**
     * Extracts geodetic point id from noteData if exists
     */
    public function extractGeodeticPointId($noteData): int {
        if (!array_key_exists("geodeticPointId", $noteData) || empty($noteData["geodeticPointId"])) {
            throw new Exception("No geodetic point data provided in the request body");
        }

        return (int) $noteData["geodeticPointId"];
    }

    /**
     * Extracts user id from noteData if exists
     */
    public function extractUserId($noteData): int {
        if (!array_key_exists("userId", $noteData) || empty($noteData["userId"])) {
            throw new Exception("No user data provided in the request body");
        }

        return (int) $noteData["userId"];
    }

    /**
     * @return Note[]
     */
    public function findAll(int $fromId, int $limit, string $lastModified, array $userIds = [], ?int $lifecyclePhase): array
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('n')
            ->from('Note', 'n')
            ->innerJoin('n.mission', 'm')
            ->where('n.id >= ?1 AND n.modified >= ?2')
            ->orderBy('n.id', 'ASC')
            ->setMaxResults($limit + 1)
            ->setParameter(1, $fromId)
            ->setParameter(2, $lastModified);
        
        if (count($userIds) > 0) {
            $qb->andWhere('n.userId IN (:userIds)')
                ->setParameter('userIds', $userIds);
        }

        if (isset($lifecyclePhase)) {
            $qb->andWhere('m.lifecyclePhase = ?3')
                ->setParameter(3, $lifecyclePhase);
        }

        $notes = $qb->getQuery()->getResult();

        return $notes;
    }

    /**
     * Returns count of entities
     */
    public function getCount(?string $lastModified, array $userIds = [], ?int $lifecyclePhase): int {
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(n.id)')
            ->from('Note', 'n')
            ->innerJoin('n.mission', 'm');
        
        if (!empty($lastModified)) {
            $qb->andWhere('n.modified >= ?1')
                ->setParameter(1, $lastModified);
        }
        if (count($userIds) > 0) {
            $qb->andWhere('n.userId IN (:userIds)')
                ->setParameter('userIds', $userIds);
        }

        if (isset($lifecyclePhase)) {
            $qb->andWhere('m.lifecyclePhase = ?2')
                ->setParameter(2, $lifecyclePhase);
        }

        $count = $qb->getQuery()->getSingleScalarResult();

        return (int) $count;
    }
}
