<?php
declare(strict_types=1);

namespace App\Domain\GeodeticPointMission;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Exception;
use DateTime;
use App\Domain\Validator\DateTimeValidator;
use App\Domain\Validator\RoomToMariaDb;

class GeodeticPointMissionRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    private $defaultGeodeticPointMissionData = array(
        "missionId" => null,
        "geodeticPointId" => null,
        "userId" => null,
        "groupId" => null,
        "status" => null,
        "created" => null,
        "modified" => null
    );

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return GeodeticPointMission
     */
    public function synchronizeGeodeticPointMission(array $geodeticPointMissionData) : \GeodeticPointMission {
        // $this->logger->info("Changed data: " . json_encode($geodeticPointMissionData));
        $missionId = empty($geodeticPointMissionData["missionId"]) ? null : $geodeticPointMissionData["missionId"];
        $geodeticPointId = empty($geodeticPointMissionData["geodeticPointId"]) ? null : $geodeticPointMissionData["geodeticPointId"];
        $groupId = empty($geodeticPointMissionData["groupId"]) ? null : $geodeticPointMissionData["groupId"];
        $geodeticPointMissionEntity = $this->em->getRepository('GeodeticPointMission')->findOneBy(array(
            "missionId" => $missionId,
            "geodeticPointId" => $geodeticPointId,
            "groupId" => $groupId
        ));

        if (!empty($geodeticPointMissionEntity)) {
            // update existing point
            $geodeticPointMissionEntity = $this->updateGeodeticPointMission($geodeticPointMissionData, $geodeticPointMissionEntity);
        } else {
            // insert new point
            $geodeticPointMissionEntity = $this->insertGeodeticPointMission($geodeticPointMissionData);
        }

        return $geodeticPointMissionEntity;
    }

    /**
     * @return GeodeticPointMission
     */
    public function insertGeodeticPointMission(array $geodeticPointMissionData): \GeodeticPointMission
    {
        $missionId = $this->extractMissionId($geodeticPointMissionData);
        $missionEntity = $this->em->getRepository('Mission')->findOneBy(array("id" => $missionId));
        if (empty($missionEntity)) {
            throw new Exception("Mission entity does not exist for id " . $missionId);
        }

        $geodeticPointId = $this->extractGeodeticPointId($geodeticPointMissionData);
        $geodeticPointEntity = $this->em->getRepository('GeodeticPoint')->findOneBy(array("id" => $geodeticPointId));
        if (empty($geodeticPointEntity)) {
            throw new Exception("GeodeticPoint entity does not exist for id " . $geodeticPointId);
        }

        $userId = $this->extractUserId($geodeticPointMissionData);

        $groupId = $this->extractGroupId($geodeticPointMissionData);

        $harmonizedData = $this->harmonizeData($geodeticPointMissionData);
        
        $newGeodeticPointMission = new \GeodeticPointMission();
        $newGeodeticPointMission->setMissionId($missionId);
        $newGeodeticPointMission->setGeodeticPointId($geodeticPointId);
        $newGeodeticPointMission->setUserId($userId);
        $newGeodeticPointMission->setGroupId($groupId);
        $newGeodeticPointMission->setStatus($harmonizedData["status"]);
        $newGeodeticPointMission->setCreated($harmonizedData["created"]);
        $newGeodeticPointMission->setModified($harmonizedData["modified"]);
        $newGeodeticPointMission->setMission($missionEntity);
        $newGeodeticPointMission->setGeodeticPoint($geodeticPointEntity);
        
        $this->logger->info('I am here with final gpm data: ' . json_encode($newGeodeticPointMission));

        $this->em->persist($newGeodeticPointMission);
        $this->em->flush();

        return $newGeodeticPointMission;
    }

    /**
     * @return GeodeticPointMission
     */
    public function updateGeodeticPointMission(array $geodeticPointMissionData, \GeodeticPointMission $geodeticPointMissionEntity = null): \GeodeticPointMission {
        $missionId = $this->extractMissionId($geodeticPointMissionData);
        $geodeticPointId = $this->extractGeodeticPointId($geodeticPointMissionData);
        $userId = $this->extractUserId($geodeticPointMissionData);
        $groupId = $this->extractGroupId($geodeticPointMissionData);
        
        $harmonizedData = $this->harmonizeData($geodeticPointMissionData);

        if (empty($geodeticPointMissionEntity)) {
            $geodeticPointMissionEntity = $this->em->getRepository('GeodeticPointMission')->findOneBy(array(
                "missionId" => $missionId,
                "geodeticPointId" => $geodeticPointId,
                "groupId" => $groupId
            ));
        }
        
        $geodeticPointMissionEntity->setMissionId($missionId);
        $geodeticPointMissionEntity->setGeodeticPointId($geodeticPointId);
        $geodeticPointMissionEntity->setUserId($userId);
        $geodeticPointMissionEntity->setGroupId($groupId);
        $geodeticPointMissionEntity->setStatus($harmonizedData["status"]);
        $geodeticPointMissionEntity->setCreated($harmonizedData["created"]);
        $geodeticPointMissionEntity->setModified($harmonizedData["modified"]);

        $this->em->flush();

        return $geodeticPointMissionEntity;
    }

    /**
     * Transforms data to be insertable
     * @return array
     */
    public function harmonizeData(array $geodeticPointMissionData): array {
        if (array_key_exists("created", $geodeticPointMissionData) && DateTimeValidator::isValid($geodeticPointMissionData["created"])) {
            $geodeticPointMissionData["created"] = new DateTime($geodeticPointMissionData["created"]);
        } else {
            unset($geodeticPointMissionData["created"]);
        }
        if (array_key_exists("modified", $geodeticPointMissionData) && DateTimeValidator::isValid($geodeticPointMissionData["modified"])) {
            $geodeticPointMissionData["modified"] = new DateTime($geodeticPointMissionData["modified"]);
        } else {
            unset($geodeticPointMissionData["modified"]);
        }

        $harmonizedData = array_replace($this->defaultGeodeticPointMissionData, $geodeticPointMissionData);
        return $harmonizedData;
    }

    /**
     * Extracts mission id from geodeticPointMissionData if exists
     */
    public function extractMissionId($geodeticPointMissionData): int {
        if (!array_key_exists("missionId", $geodeticPointMissionData) || empty($geodeticPointMissionData["missionId"])) {
            throw new Exception("No mission data provided in the request body");
        }

        return (int) $geodeticPointMissionData["missionId"];
    }

    /**
     * Extracts geodetic point id from geodeticPointMissionData if exists
     */
    public function extractGeodeticPointId($geodeticPointMissionData): int {
        if (!array_key_exists("geodeticPointId", $geodeticPointMissionData) || empty($geodeticPointMissionData["geodeticPointId"])) {
            throw new Exception("No geodetic point data provided in the request body");
        }

        return (int) $geodeticPointMissionData["geodeticPointId"];
    }

    /**
     * Extracts user id from geodeticPointMissionData if exists
     */
    public function extractUserId($geodeticPointMissionData): ?int {
        if (!array_key_exists("userId", $geodeticPointMissionData) || empty($geodeticPointMissionData["userId"])) {
            return null;
        }

        return (int) $geodeticPointMissionData["userId"];
    }

    /**
     * Extracts group id from geodeticPointTaskData if exists
     */
    public function extractGroupId($geodeticPointMissionData): ?int {
        if (!array_key_exists("groupId", $geodeticPointMissionData) || empty($geodeticPointMissionData["groupId"])) {
            return null;
        }

        return (int) $geodeticPointMissionData["groupId"];
    }

    /**
     * @return GeodeticPointMission[]
     */
    public function findAll(int $fromId, int $limit, string $lastModified, ?int $userId, ?array $groupIds, ?int $lifecyclePhase): array
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('gpm')
            ->from('GeodeticPointMission', 'gpm')
            ->innerJoin('gpm.geodeticPoint', 'gp')
            ->innerJoin('gpm.mission', 'm')
            ->where('gpm.id >= ?1 AND gpm.modified >= ?2')
            ->orderBy('gpm.id', 'ASC')
            ->setMaxResults($limit + 1)
            ->setParameter(1, $fromId)
            ->setParameter(2, $lastModified);
        
        if (!empty($userId)) {
            $qb->andWhere('gpm.userId = ?3')
                ->setParameter(3, $userId);
        }

        if (!empty($groupIds)) {
            $qb->andWhere('gpm.groupId IN (:groupIds)')
                ->setParameter('groupIds', $groupIds);
        }

        if (isset($lifecyclePhase)) {
            $qb->andWhere('m.lifecyclePhase = ?4')
                ->setParameter(4, $lifecyclePhase);
        }

        $geodeticPointMissions = $qb->getQuery()->getResult();

        return $geodeticPointMissions;
    }

    /**
     * Returns count of entities
     */
    public function getCount(?string $lastModified, ?int $userId, ?array $groupIds, ?int $lifecyclePhase): int {
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(gpm.id)')
            ->from('GeodeticPointMission', 'gpm')
            ->innerJoin('gpm.mission', 'm');
        
        if (!empty($lastModified)) {
            $qb->andWhere('gpm.modified >= ?3')
                ->setParameter(3, $lastModified);
        }
        if (!empty($userId)) {
            $qb->andWhere('gpm.userId = ?4')
                ->setParameter(4, $userId);
        }

        if (!empty($groupIds)) {
            $qb->andWhere('gpm.groupId IN (:groupIds)')
                ->setParameter('groupIds', $groupIds);
        }

        if (isset($lifecyclePhase)) {
            $qb->andWhere('m.lifecyclePhase = ?5')
                ->setParameter(5, $lifecyclePhase);
        }

        $count = $qb->getQuery()->getSingleScalarResult();

        return (int) $count;
    }
}
