<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GeodeticPoint
 *
 * @ORM\Table(name="geodetic_points")
 * @ORM\Entity
 */
class GeodeticPoint implements JsonSerializable
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="label", type="text", length=65535, nullable=true)
     */
    private $label;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="text", length=65535, nullable=true)
     */
    private $type;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_maintenance", type="datetime", nullable=true)
     */
    private $lastMaintenance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $modified = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set label.
     *
     * @param string|null $label
     *
     * @return GeodeticPoint
     */
    public function setLabel($label = null)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string|null
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set type.
     *
     * @param string|null $type
     *
     * @return GeodeticPoint
     */
    public function setType($type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set lastMaintenance.
     *
     * @param \DateTime|null $lastMaintenance
     *
     * @return GeodeticPoint
     */
    public function setLastMaintenance($lastMaintenance = null)
    {
        $this->lastMaintenance = $lastMaintenance;

        return $this;
    }

    /**
     * Get lastMaintenance.
     *
     * @return \DateTime|null
     */
    public function getLastMaintenance()
    {
        return $this->lastMaintenance;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return GeodeticPoint
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified.
     *
     * @param \DateTime $modified
     *
     * @return GeodeticPoint
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified.
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            'type' => $this->type,
            'lastMaintenance' => $this->lastMaintenance,
            'created' => $this->created,
            'modified' => $this->modified,
        ];
    }
}
