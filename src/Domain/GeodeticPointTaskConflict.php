<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GeodeticPointTaskConflict
 *
 * @ORM\Table(name="geodetic_point_task_conflicts", indexes={@ORM\Index(name="mission_id", columns={"mission_id"}), @ORM\Index(name="group_id", columns={"group_id"}), @ORM\Index(name="geodetic_point_id", columns={"geodetic_point_id"}), @ORM\Index(name="geodetic_point_task_id", columns={"geodetic_point_task_id"}), @ORM\Index(name="task_id", columns={"task_id"})})
 * @ORM\Entity
 */
class GeodeticPointTaskConflict implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="geodetic_point_task_id", type="integer", nullable=false)
     */
    private $geodeticPointTaskId;

    /**
     * @var int
     *
     * @ORM\Column(name="mission_id", type="integer", nullable=false)
     */
    private $missionId;

    /**
     * @var int
     *
     * @ORM\Column(name="geodetic_point_id", type="integer", nullable=false)
     */
    private $geodeticPointId;

    /**
     * @var int
     *
     * @ORM\Column(name="task_id", type="integer", nullable=false)
     */
    private $taskId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="group_id", type="bigint", nullable=true)
     */
    private $groupId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=true)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="is_mandatory", type="integer", nullable=false)
     */
    private $isMandatory;

    /**
     * @var int
     *
     * @ORM\Column(name="is_done", type="integer", nullable=false)
     */
    private $isDone;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="done_date", type="datetime", nullable=true)
     */
    private $doneDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $modified = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \GeodeticPointTask
     *
     * @ORM\OneToOne(targetEntity="GeodeticPointTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="geodetic_point_task_id", referencedColumnName="id", unique=true)
     * })
     */
    private $geodeticPointTask;

    /**
     * @var \GeodeticPoint
     *
     * @ORM\OneToOne(targetEntity="GeodeticPoint")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="geodetic_point_id", referencedColumnName="id", unique=true)
     * })
     */
    private $geodeticPoint;

    /**
     * @var \Mission
     *
     * @ORM\OneToOne(targetEntity="Mission")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mission_id", referencedColumnName="id", unique=true)
     * })
     */
    private $mission;

    /**
     * @var \Task
     *
     * @ORM\OneToOne(targetEntity="Task")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id", unique=true)
     * })
     */
    private $task;

    /**
     * @var \Group
     *
     * @ORM\OneToOne(targetEntity="Group")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="group_id", referencedColumnName="id", unique=true)
     * })
     */
    private $group;


    /**
     * Set geodeticPointTaskId.
     *
     * @param int $geodeticPointTaskId
     *
     * @return GeodeticPointTaskConflict
     */
    public function setGeodeticPointTaskId($geodeticPointTaskId)
    {
        $this->geodeticPointTaskId = $geodeticPointTaskId;

        return $this;
    }

    /**
     * Get geodeticPointTaskId.
     *
     * @return int
     */
    public function getGeodeticPointTaskId()
    {
        return $this->geodeticPointTaskId;
    }

    /**
     * Set missionId.
     *
     * @param int $missionId
     *
     * @return GeodeticPointTaskConflict
     */
    public function setMissionId($missionId)
    {
        $this->missionId = $missionId;

        return $this;
    }

    /**
     * Get missionId.
     *
     * @return int
     */
    public function getMissionId()
    {
        return $this->missionId;
    }

    /**
     * Set geodeticPointId.
     *
     * @param int $geodeticPointId
     *
     * @return GeodeticPointTaskConflict
     */
    public function setGeodeticPointId($geodeticPointId)
    {
        $this->geodeticPointId = $geodeticPointId;

        return $this;
    }

    /**
     * Get geodeticPointId.
     *
     * @return int
     */
    public function getGeodeticPointId()
    {
        return $this->geodeticPointId;
    }

    /**
     * Set taskId.
     *
     * @param int $taskId
     *
     * @return GeodeticPointTaskConflict
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId.
     *
     * @return int
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set groupId.
     *
     * @param int|null $groupId
     *
     * @return GeodeticPointTaskConflict
     */
    public function setGroupId($groupId = null)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId.
     *
     * @return int|null
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return GeodeticPointTaskConflict
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set isMandatory.
     *
     * @param int $isMandatory
     *
     * @return GeodeticPointTaskConflict
     */
    public function setIsMandatory($isMandatory)
    {
        $this->isMandatory = $isMandatory;

        return $this;
    }

    /**
     * Get isMandatory.
     *
     * @return int
     */
    public function getIsMandatory()
    {
        return $this->isMandatory;
    }

    /**
     * Set isDone.
     *
     * @param int $isDone
     *
     * @return GeodeticPointTaskConflict
     */
    public function setIsDone($isDone)
    {
        $this->isDone = $isDone;

        return $this;
    }

    /**
     * Get isDone.
     *
     * @return int
     */
    public function getIsDone()
    {
        return $this->isDone;
    }

    /**
     * Set doneDate.
     *
     * @param \DateTime|null $doneDate
     *
     * @return GeodeticPointTaskConflict
     */
    public function setDoneDate($doneDate = null)
    {
        $this->doneDate = $doneDate;

        return $this;
    }

    /**
     * Get doneDate.
     *
     * @return \DateTime|null
     */
    public function getDoneDate()
    {
        return $this->doneDate;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return GeodeticPointTaskConflict
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified.
     *
     * @param \DateTime $modified
     *
     * @return GeodeticPointTaskConflict
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified.
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set geodeticPointTask.
     *
     * @param \GeodeticPointTask|null $geodeticPointTask
     *
     * @return GeodeticPointTaskConflict
     */
    public function setGeodeticPointTask(\GeodeticPointTask $geodeticPointTask = null)
    {
        $this->geodeticPointTask = $geodeticPointTask;

        return $this;
    }

    /**
     * Get geodeticPointTask.
     *
     * @return \GeodeticPointTask|null
     */
    public function getGeodeticPointTask()
    {
        return $this->geodeticPointTask;
    }

    /**
     * Set geodeticPoint.
     *
     * @param \GeodeticPoint|null $geodeticPoint
     *
     * @return GeodeticPointTaskConflict
     */
    public function setGeodeticPoint(\GeodeticPoint $geodeticPoint = null)
    {
        $this->geodeticPoint = $geodeticPoint;

        return $this;
    }

    /**
     * Get geodeticPoint.
     *
     * @return \GeodeticPoint|null
     */
    public function getGeodeticPoint()
    {
        return $this->geodeticPoint;
    }

    /**
     * Set mission.
     *
     * @param \Mission|null $mission
     *
     * @return GeodeticPointTaskConflict
     */
    public function setMission(\Mission $mission = null)
    {
        $this->mission = $mission;

        return $this;
    }

    /**
     * Get mission.
     *
     * @return \Mission|null
     */
    public function getMission()
    {
        return $this->mission;
    }

    /**
     * Set task.
     *
     * @param \Task|null $task
     *
     * @return GeodeticPointTaskConflict
     */
    public function setTask(\Task $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task.
     *
     * @return \Task|null
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set group.
     *
     * @param \Group|null $group
     *
     * @return GeodeticPointTaskConflict
     */
    public function setGroup(\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group.
     *
     * @return \Group|null
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'geodeticPointTaskId' => $this->geodeticPointTaskId,
            'missionId' => $this->missionId,
            'geodeticPointId' => $this->geodeticPointId,
            'taskId' => $this->taskId,
            'userId' => $this->userId,
            'groupId' => $this->groupId,
            'isMandatory' => $this->isMandatory,
            'isDone' => $this->isDone,
            'doneDate' => $this->doneDate,
            'created' => $this->created,
            'modified' => $this->modified,
            'geodeticPoint' => $this->geodeticPoint,
            'mission' => $this->mission,
            'task' => $this->task,
        ];
    }
}
