<?php
declare(strict_types=1);

namespace App\Domain\GeodeticPoint;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use DateTime;
use App\Domain\Validator\DateTimeValidator;
use App\Domain\Validator\RoomToMariaDb;

class GeodeticPointRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    private $defaultGeodeticPointData = array(
        "id" => null,
        "label" => null,
        "type" => null,
        "lastMaintenance" => null,
        "created" => null,
        "modified" => null
    );

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return GeodeticPoint
     */
    public function synchronizeGeodeticPoint(array $geodeticPointData) : \GeodeticPoint {
        $this->logger->info("Changed data: " . json_encode($geodeticPointData));
        $label = empty($geodeticPointData["label"]) ? null : $geodeticPointData["label"];
        $geodeticPointEntity = $this->em->getRepository('GeodeticPoint')->findOneBy(array("label" => $label));

        if (!empty($geodeticPointEntity)) {
            // update existing point
            $geodeticPointEntity = $this->updateGeodeticPoint((int) $geodeticPointEntity->getId(), $geodeticPointData, $geodeticPointEntity);
        } else {
            // insert new point
            $geodeticPointEntity = $this->insertGeodeticPoint($geodeticPointData);
        }

        return $geodeticPointEntity;
    }

    /**
     * @return GeodeticPoint
     */
    public function insertGeodeticPoint(array $geodeticPointData): \GeodeticPoint
    {        
        $this->logger->info("Type of incoming point: " . $geodeticPointData["type"]);
        $harmonizedData = $this->harmonizeData($geodeticPointData);
        
        $geodeticPointEntity = new \GeodeticPoint();
        $geodeticPointEntity->setLabel($harmonizedData["label"]);
        $geodeticPointEntity->setType($harmonizedData["type"]);
        $geodeticPointEntity->setLastMaintenance($harmonizedData["lastMaintenance"]);
        $geodeticPointEntity->setCreated($harmonizedData["created"]);
        $geodeticPointEntity->setModified($harmonizedData["modified"]);

        $this->em->persist($geodeticPointEntity);
        $this->em->flush();

        return $geodeticPointEntity;
    }

    public function updateGeodeticPoint(int $id, array $geodeticPointData, \GeodeticPoint $geodeticPointEntity = null): \GeodeticPoint {
        $harmonizedData = $this->harmonizeData($geodeticPointData);

        if (empty($geodeticPointEntity)) {
            $geodeticPointEntity = $this->em->getRepository('GeodeticPoint')->findOneBy(array("id" => $id));
        }
        
        $geodeticPointEntity->setLabel($harmonizedData["label"]);
        $geodeticPointEntity->setType($harmonizedData["type"]);
        $geodeticPointEntity->setLastMaintenance($harmonizedData["lastMaintenance"]);
        $geodeticPointEntity->setModified($harmonizedData["modified"]);

        $this->em->flush();

        return $geodeticPointEntity;
    }

    /**
     * Transforms data to be insertable
     */
    public function harmonizeData(array $geodeticPointData): array {
        if (array_key_exists("lastMaintenance", $geodeticPointData) && DateTimeValidator::isValid($geodeticPointData["lastMaintenance"])) {
            $geodeticPointData["lastMaintenance"] = new DateTime($geodeticPointData["lastMaintenance"]);
        } else {
            unset($geodeticPointData["lastMaintenance"]);
        }
        if (array_key_exists("created", $geodeticPointData) && DateTimeValidator::isValid($geodeticPointData["created"])) {
            $geodeticPointData["created"] = new DateTime($geodeticPointData["created"]);
        } else {
            unset($geodeticPointData["created"]);
        }
        if (array_key_exists("modified", $geodeticPointData) && DateTimeValidator::isValid($geodeticPointData["modified"])) {
            $geodeticPointData["modified"] = new DateTime($geodeticPointData["modified"]);
        } else {
            unset($geodeticPointData["modified"]);
        }

        $harmonizedData = array_replace($this->defaultGeodeticPointData, $geodeticPointData);
        return $harmonizedData;
    }

    /**
     * @return GeodeticPoint[]
     */
    public function findAll(): array
    {
        $this->logger->info('Before get');
        $geodeticPoints = $this->em->getRepository('GeodeticPoint')->findAll();
        $this->logger->info("Getting geodetic points " . serialize($geodeticPoints));

        return $geodeticPoints;
    }
}
