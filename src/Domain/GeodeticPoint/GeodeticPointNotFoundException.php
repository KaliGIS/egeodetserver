<?php
declare(strict_types=1);

namespace App\Domain\GeodeticPoint;

use App\Domain\DomainException\DomainRecordNotFoundException;

class GeodeticPointNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The geodetic point you requested does not exist.';
}
