<?php

namespace App\Domain\Validator;

class RoomToMariaDb {
    /**
     * Room (Android) objects has prefix 'm', e.g. 'id' comes as 'mId'
     * This function changes 1D associative arrays keys by removing 'm' prefixes and changes 
     * the first letter to lowercase.
     */
    public static function changeKeys(array $roomData): array {
        $mariaDbData = array();

        $roomKeys = array_keys($roomData);
        foreach($roomKeys as $roomKey) {
            if (substr($roomKey, 0, 1) == "m" && strlen($roomKey) > 1) {
                $mariaDbKey = lcfirst(substr($roomKey, 1));
            } else {
                $mariaDbKey = $roomKey;
            }

            $mariaDbData[$mariaDbKey] = $roomData[$roomKey];
        }

        return $mariaDbData;
    }

    /**
     * Change Room datetime value to MariaDb datetime value
     * example of Room format Dec 7, 2020 5:37:44 PM
     */
    public static function changeDatetimeValue(array $roomData, string $datetimeKey, string $roomFormat = 'M j, Y g:i:s A'): array {
        if (array_key_exists($datetimeKey, $roomData)) {
            $dateTime = DateTime::createFromFormat($roomFormat, $roomData[$datetimeKey]);
            $roomData[$datetimeKey] = $dateTime->format('Y-m-d H:i:s');
        }
        
        return $roomData;
    }
}