<?php

namespace App\Domain\Validator;

use DateTime;

class DateTimeValidator {
    public static function isValid($dateTime, $format = 'Y-m-d H:i:s'){
        $d = DateTime::createFromFormat($format, $dateTime);
        return $d && $d->format($format) === $dateTime;
    }
}