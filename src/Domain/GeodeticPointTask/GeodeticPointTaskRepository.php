<?php
declare(strict_types=1);

namespace App\Domain\GeodeticPointTask;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Exception;
use DateTime;
use App\Domain\Validator\DateTimeValidator;
use App\Domain\Validator\RoomToMariaDb;

class GeodeticPointTaskRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    private $defaultGeodeticPointTaskData = array(
        "missionId" => null,
        "geodeticPointId" => null,
        "taskId" => null,
        "userId" => null,
        "groupId" => null,
        "isMandatory" => 0,
        "isDone" => 0,
        "doneDate" => null,
        "created" => null,
        "modified" => null,
    );

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * Checks if record exists to know if update or insert
     * @return boolean
     */
    public function recordExists(array $geodeticPointTaskData) : ?\GeodeticPointTask {
        $id = empty($geodeticPointTaskData["id"]) ? null : $geodeticPointTaskData["id"];
        $missionId = empty($geodeticPointTaskData["missionId"]) ? null : $geodeticPointTaskData["missionId"];
        $taskId = empty($geodeticPointTaskData["taskId"]) ? null : $geodeticPointTaskData["taskId"];
        $geodeticPointId = empty($geodeticPointTaskData["geodeticPointId"]) ? null : $geodeticPointTaskData["geodeticPointId"];
        $geodeticPointTaskEntity = $this->em->getRepository('GeodeticPointTask')->findOneBy(array("id" => $id));
        $this->logger->info('Finding by ID: ' . json_encode($id));
        $this->logger->info('Geodetic Point Task Entity: ' . json_encode($geodeticPointTaskEntity));

        if (!empty($geodeticPointTaskEntity)
            && $geodeticPointTaskEntity->getMissionId() == $missionId
            && $geodeticPointTaskEntity->getTaskId() == $taskId
            && $geodeticPointTaskEntity->getGeodeticPointId() == $geodeticPointId
        ) {
            return $geodeticPointTaskEntity;
        }

        return null;
    }

    // /**
    //  * @return GeodeticPointTask
    //  */
    // public function synchronizeGeodeticPointTask(array $geodeticPointTaskData) : \GeodeticPointTask {
    //     // $this->logger->info("Changed data: " . json_encode($geodeticPointTaskData));
    //     $id = empty($geodeticPointTaskData["id"]) ? null : $geodeticPointTaskData["id"];
    //     $missionId = empty($geodeticPointTaskData["missionId"]) ? null : $geodeticPointTaskData["missionId"];
    //     $taskId = empty($geodeticPointTaskData["taskId"]) ? null : $geodeticPointTaskData["taskId"];
    //     $geodeticPointId = empty($geodeticPointTaskData["geodeticPointId"]) ? null : $geodeticPointTaskData["geodeticPointId"];
    //     $geodeticPointTaskEntity = $this->em->getRepository('GeodeticPointTask')->findOneBy(array("id" => $id));

    //     if (!empty($geodeticPointTaskEntity)
    //         && $geodeticPointTaskEntity->getMissionId() == $missionId
    //         && $geodeticPointTaskEntity->getTaskId() == $taskId
    //         && $geodeticPointTaskEntity->getGeodeticPointId() == $geodeticPointId
    //     ) {
    //         // update existing point
    //         $geodeticPointTaskEntity = $this->updateGeodeticPointTask((int) $geodeticPointTaskEntity->getId(), $geodeticPointTaskData, $geodeticPointTaskEntity);
    //     } else {
    //         // insert new point
    //         $geodeticPointTaskEntity = $this->insertGeodeticPointTask($geodeticPointTaskData);
    //     }

    //     return $geodeticPointTaskEntity;
    // }

    /**
     * Checks if the record exist with the same user id in DB
     * @return boolean
     */
    public function isUserIdSame(array $geodeticPointTaskData, \GeodeticPointTask $geodeticPointTaskEntity) : bool {
        $userId = empty($geodeticPointTaskData["userId"]) ? null : $geodeticPointTaskData["userId"];
        if (empty($userId)) {
            return true;
        } elseif (empty($geodeticPointTaskEntity)) {
            return true;
        } elseif ($geodeticPointTaskEntity->getUserId() == null) {
            return true;
        } elseif ($geodeticPointTaskEntity->getUserId() == $userId) {
            return true;
        }
        
        return false;
    }

    /**
     * @return GeodeticPointTask
     */
    public function insertGeodeticPointTask(array $geodeticPointTaskData): \GeodeticPointTask
    {
        $missionId = $this->extractMissionId($geodeticPointTaskData);
        $missionEntity = $this->em->getRepository('Mission')->findOneBy(array("id" => $missionId));
        if (empty($missionEntity)) {
            throw new Exception("Mission entity does not exist for id " . $missionId);
        }

        $geodeticPointId = $this->extractGeodeticPointId($geodeticPointTaskData);
        $geodeticPointEntity = $this->em->getRepository('GeodeticPoint')->findOneBy(array("id" => $geodeticPointId));
        if (empty($geodeticPointEntity)) {
            throw new Exception("GeodeticPoint entity does not exist for id " . $geodeticPointId);
        }

        $taskId = $this->extractTaskId($geodeticPointTaskData);
        $taskEntity = $this->em->getRepository('Task')->findOneBy(array("id" => $taskId));
        if (empty($taskEntity)) {
            throw new Exception("Task entity does not exist for id " . $taskId);
        }

        $userId = $this->extractUserId($geodeticPointTaskData);

        $groupId = $this->extractGroupId($geodeticPointTaskData);
        
        $harmonizedData = $this->harmonizeData($geodeticPointTaskData);

        $geodeticPointTaskEntity = new \GeodeticPointTask();
        $geodeticPointTaskEntity->setMissionId($missionId);
        $geodeticPointTaskEntity->setGeodeticPointId($geodeticPointId);
        $geodeticPointTaskEntity->setTaskId($taskId);
        $geodeticPointTaskEntity->setUserId($userId);
        $geodeticPointTaskEntity->setGroupId($groupId);
        $geodeticPointTaskEntity->setIsMandatory($harmonizedData["isMandatory"]);
        $geodeticPointTaskEntity->setIsDone($harmonizedData["isDone"]);
        $geodeticPointTaskEntity->setDoneDate($harmonizedData["doneDate"]);
        $geodeticPointTaskEntity->setCreated($harmonizedData["created"]);
        $geodeticPointTaskEntity->setModified($harmonizedData["modified"]);    
        $geodeticPointTaskEntity->setMission($missionEntity);
        $geodeticPointTaskEntity->setGeodeticPoint($geodeticPointEntity);
        $geodeticPointTaskEntity->setTask($taskEntity);
        
        $this->logger->info('I am here with final gpt data: ' . json_encode($geodeticPointTaskEntity));
    
        $this->em->persist($geodeticPointTaskEntity);
        $this->em->flush();

        return $geodeticPointTaskEntity;
    }

    /**
     * @return GeodeticPointTask
     */
    public function updateGeodeticPointTask(int $id, array $geodeticPointTaskData, \GeodeticPointTask $geodeticPointTaskEntity = null): \GeodeticPointTask {
        $missionId = $this->extractMissionId($geodeticPointTaskData);
        $geodeticPointId = $this->extractGeodeticPointId($geodeticPointTaskData);
        $taskId = $this->extractTaskId($geodeticPointTaskData);
        $userId = $this->extractUserId($geodeticPointTaskData);
        $groupId = $this->extractGroupId($geodeticPointTaskData);
        
        $harmonizedData = $this->harmonizeData($geodeticPointTaskData);

        if (empty($geodeticPointTaskEntity)) {
            $geodeticPointTaskEntity = $this->em->getRepository('GeodeticPointTask')->findOneBy(array("id" => $id));
        }
        
        $geodeticPointTaskEntity->setMissionId($missionId);
        $geodeticPointTaskEntity->setGeodeticPointId($geodeticPointId);
        $geodeticPointTaskEntity->setTaskId($taskId);
        $geodeticPointTaskEntity->setUserId($userId);
        $geodeticPointTaskEntity->setGroupId($groupId);
        $geodeticPointTaskEntity->setIsMandatory($harmonizedData["isMandatory"]);
        $geodeticPointTaskEntity->setIsDone($harmonizedData["isDone"]);
        $geodeticPointTaskEntity->setDoneDate($harmonizedData["doneDate"]);
        $geodeticPointTaskEntity->setCreated($harmonizedData["created"]);
        $geodeticPointTaskEntity->setModified($harmonizedData["modified"]);

        $this->em->flush();

        return $geodeticPointTaskEntity;
    }

    /**
     * Transforms data to be insertable
     * @return array
     */
    public function harmonizeData(array $geodeticPointTaskData): array {
        if (array_key_exists("doneDate", $geodeticPointTaskData) && DateTimeValidator::isValid($geodeticPointTaskData["doneDate"])) {
            $geodeticPointTaskData["doneDate"] = new DateTime($geodeticPointTaskData["doneDate"]);
        } else {
            unset($geodeticPointTaskData["doneDate"]);
        }
        if (array_key_exists("created", $geodeticPointTaskData) && DateTimeValidator::isValid($geodeticPointTaskData["created"])) {
            $geodeticPointTaskData["created"] = new DateTime($geodeticPointTaskData["created"]);
        } else {
            unset($geodeticPointTaskData["created"]);
        }
        if (array_key_exists("modified", $geodeticPointTaskData) && DateTimeValidator::isValid($geodeticPointTaskData["modified"])) {
            $geodeticPointTaskData["modified"] = new DateTime($geodeticPointTaskData["modified"]);
        } else {
            unset($geodeticPointTaskData["modified"]);
        }
        if (array_key_exists("isMandatory", $geodeticPointTaskData)) {
            $geodeticPointTaskData["isMandatory"] = $geodeticPointTaskData["isMandatory"] == "true" ? 1 : 0;
        }
        if (array_key_exists("isDone", $geodeticPointTaskData)) {
            $geodeticPointTaskData["isDone"] = $geodeticPointTaskData["isDone"] == "true" ? 1 : 0;
        }

        $harmonizedData = array_replace($this->defaultGeodeticPointTaskData, $geodeticPointTaskData);
        return $harmonizedData;
    }

    /**
     * Extracts mission id from geodeticPointTaskData if exists
     */
    public function extractMissionId($geodeticPointTaskData): int {
        if (!array_key_exists("missionId", $geodeticPointTaskData) || empty($geodeticPointTaskData["missionId"])) {
            throw new Exception("No mission data provided in the request body");
        }

        return (int) $geodeticPointTaskData["missionId"];
    }

    /**
     * Extracts geodetic point id from geodeticPointTaskData if exists
     */
    public function extractGeodeticPointId($geodeticPointTaskData): int {
        if (!array_key_exists("geodeticPointId", $geodeticPointTaskData) || empty($geodeticPointTaskData["geodeticPointId"])) {
            throw new Exception("No geodetic point data provided in the request body");
        }

        return (int) $geodeticPointTaskData["geodeticPointId"];
    }

    /**
     * Extracts task id from geodeticPointTaskData if exists
     */
    public function extractTaskId($geodeticPointTaskData): int {
        if (!array_key_exists("taskId", $geodeticPointTaskData) || empty($geodeticPointTaskData["taskId"])) {
            throw new Exception("No task data provided in the request body");
        }

        return (int) $geodeticPointTaskData["taskId"];
    }

    /**
     * Extracts user id from geodeticPointTaskData if exists
     */
    public function extractUserId($geodeticPointTaskData): ?int {
        if (!array_key_exists("userId", $geodeticPointTaskData) || empty($geodeticPointTaskData["userId"])) {
            return null;
        }

        return (int) $geodeticPointTaskData["userId"];
    }

    /**
     * Extracts group id from geodeticPointTaskData if exists
     */
    public function extractGroupId($geodeticPointTaskData): ?int {
        if (!array_key_exists("groupId", $geodeticPointTaskData) || empty($geodeticPointTaskData["groupId"])) {
            return null;
        }

        return (int) $geodeticPointTaskData["groupId"];
    }

    /**
     * @return GeodeticPointTask[]
     */
    public function findAll(int $fromId, int $limit, string $lastModified, ?int $userId, ?array $groupIds, ?int $lifecyclePhase): array
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('gpt')
            ->from('GeodeticPointTask', 'gpt')
            ->innerJoin('gpt.geodeticPoint', 'gp')
            ->innerJoin('gpt.mission', 'm')
            ->innerJoin('gpt.task', 't')
            ->where('gpt.id >= ?1 AND gpt.modified >= ?2')
            ->orderBy('gpt.id', 'ASC')
            ->setMaxResults($limit + 1)
            ->setParameter(1, $fromId)
            ->setParameter(2, $lastModified);
        
        if (!empty($userId)) {
            $qb->andWhere('gpt.userId = ?3')
                ->setParameter(3, $userId);
        }

        if (!empty($groupIds)) {
            $qb->andWhere('gpt.groupId IN (:groupIds)')
                ->setParameter('groupIds', $groupIds);
        }

        if (isset($lifecyclePhase)) {
            $qb->andWhere('m.lifecyclePhase = ?4')
                ->setParameter(4, $lifecyclePhase);
        }

        $geodeticPointTasks = $qb->getQuery()->getResult();

        return $geodeticPointTasks;
    }

    /**
     * Returns count of entities
     */
    public function getCount(?string $lastModified, ?int $userId, ?array $groupIds, ?int $lifecyclePhase): int {
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(gpt.id)')
            ->from('GeodeticPointTask', 'gpt')
            ->innerJoin('gpt.mission', 'm');
        
        if (!empty($lastModified)) {
            $qb->andWhere('gpt.modified >= ?3')
                ->setParameter(3, $lastModified);
        }
        if (!empty($userId)) {
            $qb->andWhere('gpt.userId = ?4')
                ->setParameter(4, $userId);
        }

        if (!empty($groupIds)) {
            $qb->andWhere('gpt.groupId IN (:groupIds)')
                ->setParameter('groupIds', $groupIds);
        }

        if (isset($lifecyclePhase)) {
            $qb->andWhere('m.lifecyclePhase = ?5')
                ->setParameter(5, $lifecyclePhase);
        }

        $count = $qb->getQuery()->getSingleScalarResult();

        return (int) $count;
    }
}
