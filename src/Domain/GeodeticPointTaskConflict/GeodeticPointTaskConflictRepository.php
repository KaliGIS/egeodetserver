<?php
declare(strict_types=1);

namespace App\Domain\GeodeticPointTaskConflict;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Exception;
use DateTime;
use App\Domain\Validator\DateTimeValidator;
use App\Domain\Validator\RoomToMariaDb;

class GeodeticPointTaskConflictRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    private $defaultGeodeticPointTaskConflictData = array(
        "missionId" => null,
        "geodeticPointId" => null,
        "taskId" => null,
        "userId" => null,
        "groupId" => null,
        "isMandatory" => 0,
        "isDone" => 0,
        "doneDate" => null,
        "created" => null,
        "modified" => null,
    );

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return GeodeticPointTaskConflict
     */
    public function synchronizeGeodeticPointTaskConflict(array $GeodeticPointTaskConflictData) : \GeodeticPointTaskConflict {
        $id = empty($GeodeticPointTaskConflictData["id"]) ? null : $GeodeticPointTaskConflictData["id"];
        $GeodeticPointTaskConflictEntity = $this->em->getRepository('GeodeticPointTaskConflict')
            ->findOneBy(array("geodeticPointTaskId" => $id));

        if (!empty($GeodeticPointTaskConflictEntity)) {
            // update existing point
            $GeodeticPointTaskConflictEntity = $this->updateGeodeticPointTaskConflict((int) $GeodeticPointTaskConflictEntity->getId(), $GeodeticPointTaskConflictData, $GeodeticPointTaskConflictEntity);
        } else {
            // insert new point
            $GeodeticPointTaskConflictEntity = $this->insertGeodeticPointTaskConflict($GeodeticPointTaskConflictData);
        }

        return $GeodeticPointTaskConflictEntity;
    }

    /**
     * @return GeodeticPointTaskConflict
     */
    public function insertGeodeticPointTaskConflict(array $geodeticPointTaskConflictData): \GeodeticPointTaskConflict
    {
        $geodeticPointTaskId = $this->extractGeodeticPointTaskId($geodeticPointTaskConflictData);
        $geodeticPointTaskEntity = $this->em->getRepository('GeodeticPointTask')->findOneBy(array("id" => $geodeticPointTaskId));
        if (empty($geodeticPointTaskEntity)) {
            throw new Exception("GeodeticPointTask entity does not exist for id " . $geodeticPointTaskId);
        }

        $missionId = $this->extractMissionId($geodeticPointTaskConflictData);
        $missionEntity = $this->em->getRepository('Mission')->findOneBy(array("id" => $missionId));
        if (empty($missionEntity)) {
            throw new Exception("Mission entity does not exist for id " . $missionId);
        }

        $geodeticPointId = $this->extractGeodeticPointId($geodeticPointTaskConflictData);
        $geodeticPointEntity = $this->em->getRepository('GeodeticPoint')->findOneBy(array("id" => $geodeticPointId));
        if (empty($geodeticPointEntity)) {
            throw new Exception("GeodeticPoint entity does not exist for id " . $geodeticPointId);
        }

        $taskId = $this->extractTaskId($geodeticPointTaskConflictData);
        $taskEntity = $this->em->getRepository('Task')->findOneBy(array("id" => $taskId));
        if (empty($taskEntity)) {
            throw new Exception("Task entity does not exist for id " . $taskId);
        }

        $userId = $this->extractUserId($geodeticPointTaskConflictData);

        $groupId = $this->extractGroupId($geodeticPointTaskConflictData);
        
        $harmonizedData = $this->harmonizeData($geodeticPointTaskConflictData);

        $geodeticPointTaskConflictEntity = new \GeodeticPointTaskConflict();
        $geodeticPointTaskConflictEntity->setGeodeticPointTaskId($geodeticPointTaskId);
        $geodeticPointTaskConflictEntity->setMissionId($missionId);
        $geodeticPointTaskConflictEntity->setGeodeticPointId($geodeticPointId);
        $geodeticPointTaskConflictEntity->setTaskId($taskId);
        $geodeticPointTaskConflictEntity->setUserId($userId);
        $geodeticPointTaskConflictEntity->setGroupId($groupId);
        $geodeticPointTaskConflictEntity->setIsMandatory($harmonizedData["isMandatory"]);
        $geodeticPointTaskConflictEntity->setIsDone($harmonizedData["isDone"]);
        $geodeticPointTaskConflictEntity->setDoneDate($harmonizedData["doneDate"]);
        $geodeticPointTaskConflictEntity->setCreated($harmonizedData["created"]);
        $geodeticPointTaskConflictEntity->setModified($harmonizedData["modified"]);    
        $geodeticPointTaskConflictEntity->setGeodeticPointTask($geodeticPointTaskEntity);
        $geodeticPointTaskConflictEntity->setMission($missionEntity);
        $geodeticPointTaskConflictEntity->setGeodeticPoint($geodeticPointEntity);
        $geodeticPointTaskConflictEntity->setTask($taskEntity);
        
        $this->logger->info('I am here with final gptc data: ' . json_encode($geodeticPointTaskConflictEntity));
    
        $this->em->persist($geodeticPointTaskConflictEntity);
        $this->em->flush();

        return $geodeticPointTaskConflictEntity;
    }

    /**
     * @return GeodeticPointTaskConflict
     */
    public function updateGeodeticPointTaskConflict(int $id, array $geodeticPointTaskConflictData, \GeodeticPointTaskConflict $geodeticPointTaskConflictEntity = null): \GeodeticPointTaskConflict {
        $geodeticPointTaskId = $this->extractGeodeticPointTaskId($geodeticPointTaskConflictData);
        $missionId = $this->extractMissionId($geodeticPointTaskConflictData);
        $geodeticPointId = $this->extractGeodeticPointId($geodeticPointTaskConflictData);
        $taskId = $this->extractTaskId($geodeticPointTaskConflictData);
        $userId = $this->extractUserId($geodeticPointTaskConflictData);
        $groupId = $this->extractGroupId($geodeticPointTaskConflictData);
        
        $harmonizedData = $this->harmonizeData($geodeticPointTaskConflictData);

        if (empty($geodeticPointTaskConflictEntity)) {
            $geodeticPointTaskConflictEntity = $this->em->getRepository('GeodeticPointTaskConflict')->findOneBy(array("geodeticPointTaskId" => $id));
        }
        
        $geodeticPointTaskConflictEntity->setGeodeticPointTaskId($geodeticPointTaskId);
        $geodeticPointTaskConflictEntity->setMissionId($missionId);
        $geodeticPointTaskConflictEntity->setGeodeticPointId($geodeticPointId);
        $geodeticPointTaskConflictEntity->setTaskId($taskId);
        $geodeticPointTaskConflictEntity->setUserId($userId);
        $geodeticPointTaskConflictEntity->setGroupId($groupId);
        $geodeticPointTaskConflictEntity->setIsMandatory($harmonizedData["isMandatory"]);
        $geodeticPointTaskConflictEntity->setIsDone($harmonizedData["isDone"]);
        $geodeticPointTaskConflictEntity->setDoneDate($harmonizedData["doneDate"]);
        $geodeticPointTaskConflictEntity->setCreated($harmonizedData["created"]);
        $geodeticPointTaskConflictEntity->setModified($harmonizedData["modified"]);

        $this->em->flush();

        return $geodeticPointTaskConflictEntity;
    }

    /**
     * Transforms data to be insertable
     * @return array
     */
    public function harmonizeData(array $geodeticPointTaskConflictData): array {
        if (array_key_exists("doneDate", $geodeticPointTaskConflictData) && DateTimeValidator::isValid($geodeticPointTaskConflictData["doneDate"])) {
            $geodeticPointTaskConflictData["doneDate"] = new DateTime($geodeticPointTaskConflictData["doneDate"]);
        } else {
            unset($geodeticPointTaskConflictData["doneDate"]);
        }
        if (array_key_exists("created", $geodeticPointTaskConflictData) && DateTimeValidator::isValid($geodeticPointTaskConflictData["created"])) {
            $geodeticPointTaskConflictData["created"] = new DateTime($geodeticPointTaskConflictData["created"]);
        } else {
            unset($geodeticPointTaskConflictData["created"]);
        }
        if (array_key_exists("modified", $geodeticPointTaskConflictData) && DateTimeValidator::isValid($geodeticPointTaskConflictData["modified"])) {
            $geodeticPointTaskConflictData["modified"] = new DateTime(); // Update modified to current dateTime
        } else {
            unset($geodeticPointTaskConflictData["modified"]);
        }
        if (array_key_exists("isMandatory", $geodeticPointTaskConflictData)) {
            $geodeticPointTaskConflictData["isMandatory"] = $geodeticPointTaskConflictData["isMandatory"] == "true" ? 1 : 0;
        }
        if (array_key_exists("isDone", $geodeticPointTaskConflictData)) {
            $geodeticPointTaskConflictData["isDone"] = $geodeticPointTaskConflictData["isDone"] == "true" ? 1 : 0;
        }

        $harmonizedData = array_replace($this->defaultGeodeticPointTaskConflictData, $geodeticPointTaskConflictData);
        return $harmonizedData;
    }

    /**
     * Extracts geodetic point task id from geodeticPointTaskConflictData if exists
     */
    public function extractGeodeticPointTaskId($geodeticPointTaskConflictData): int {
        if (!array_key_exists("id", $geodeticPointTaskConflictData) || empty($geodeticPointTaskConflictData["id"])) {
            throw new Exception("No geodetic point task data provided in the request body");
        }

        return (int) $geodeticPointTaskConflictData["id"];
    }

    /**
     * Extracts mission id from geodeticPointTaskConflictData if exists
     */
    public function extractMissionId($geodeticPointTaskConflictData): int {
        if (!array_key_exists("missionId", $geodeticPointTaskConflictData) || empty($geodeticPointTaskConflictData["missionId"])) {
            throw new Exception("No mission data provided in the request body");
        }

        return (int) $geodeticPointTaskConflictData["missionId"];
    }

    /**
     * Extracts geodetic point id from geodeticPointTaskConflictData if exists
     */
    public function extractGeodeticPointId($geodeticPointTaskConflictData): int {
        if (!array_key_exists("geodeticPointId", $geodeticPointTaskConflictData) || empty($geodeticPointTaskConflictData["geodeticPointId"])) {
            throw new Exception("No geodetic point data provided in the request body");
        }

        return (int) $geodeticPointTaskConflictData["geodeticPointId"];
    }

    /**
     * Extracts task id from geodeticPointTaskConflictData if exists
     */
    public function extractTaskId($geodeticPointTaskConflictData): int {
        if (!array_key_exists("taskId", $geodeticPointTaskConflictData) || empty($geodeticPointTaskConflictData["taskId"])) {
            throw new Exception("No task data provided in the request body");
        }

        return (int) $geodeticPointTaskConflictData["taskId"];
    }

    /**
     * Extracts user id from geodeticPointTaskConflictData if exists
     */
    public function extractUserId($geodeticPointTaskConflictData): ?int {
        if (!array_key_exists("userId", $geodeticPointTaskConflictData) || empty($geodeticPointTaskConflictData["userId"])) {
            return null;
        }

        return (int) $geodeticPointTaskConflictData["userId"];
    }

    /**
     * Extracts group id from geodeticPointTaskConflictData if exists
     */
    public function extractGroupId($geodeticPointTaskConflictData): ?int {
        if (!array_key_exists("groupId", $geodeticPointTaskConflictData) || empty($geodeticPointTaskConflictData["groupId"])) {
            return null;
        }

        return (int) $geodeticPointTaskConflictData["groupId"];
    }

    /**
     * @return GeodeticPointTaskConflict[]
     */
    public function findAll(int $fromId, int $limit, string $lastModified, ?int $userId, ?array $groupIds): array
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('gptc')
            ->from('GeodeticPointTaskConflict', 'gptc')
            ->innerJoin('gptc.geodeticPoint', 'gp')
            ->innerJoin('gptc.mission', 'm')
            ->innerJoin('gptc.task', 't')
            ->where('gptc.id >= ?1 AND gptc.modified >= ?2')
            ->orderBy('gptc.id', 'ASC')
            ->setMaxResults($limit + 1)
            ->setParameter(1, $fromId)
            ->setParameter(2, $lastModified);
        
        if (!empty($userId)) {
            $qb->andWhere('gptc.userId = ?3')
                ->setParameter(3, $userId);
        }

        if (!empty($groupIds)) {
            $qb->andWhere('gptc.groupId IN (:groupIds)')
                ->setParameter('groupIds', $groupIds);
        }

        $geodeticPointTaskConflicts = $qb->getQuery()->getResult();

        return $geodeticPointTaskConflicts;
    }

    /**
     * Returns count of entities
     */
    public function getCount(?string $lastModified, ?int $userId, ?array $groupIds): int {
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(gptc.id)')
            ->from('GeodeticPointTaskConflict', 'gptc');
        
        if (!empty($lastModified)) {
            $qb->andWhere('gptc.modified >= ?3')
                ->setParameter(3, $lastModified);
        }
        if (!empty($userId)) {
            $qb->andWhere('gptc.userId = ?4')
                ->setParameter(4, $userId);
        }

        if (!empty($groupIds)) {
            $qb->andWhere('gptc.groupId IN (:groupIds)')
                ->setParameter('groupIds', $groupIds);
        }

        $count = $qb->getQuery()->getSingleScalarResult();

        return (int) $count;
    }
}
