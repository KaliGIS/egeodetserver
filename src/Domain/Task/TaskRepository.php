<?php
declare(strict_types=1);

namespace App\Domain\Task;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class TaskRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    private $defaultTask = array(
        "name" => null,
        "type" => null,
        "created" => null,
        "modified" => null
    );

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return Task
     */
    public function insertTask(array $task): \Task
    {
        $harmonizedData = array_replace($this->defaultTask, $task);
        
        $newTask = new \Task();
        $newTask->setName($harmonizedData["name"]);
        $newTask->setType($harmonizedData["type"]);
        $newTask->setCreated($harmonizedData["created"]);
        $newTask->setModified($harmonizedData["modified"]);
        
        $this->logger->info('I am here with final task data: ' . json_encode($newTask));

        $this->em->persist($newTask);
        $this->em->flush();

        return $newTask;
    }

    /**
     * @return Task[]
     */
    public function findAll(int $fromId, int $limit, ?string $type, string $lastModified): array
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('t')
            ->from('Task', 't')
            ->where('t.id >= ?1 AND t.modified >= ?2')
            ->orderBy('t.id', 'ASC')
            ->setMaxResults($limit + 1)
            ->setParameter(1, $fromId)
            ->setParameter(2, $lastModified);
        
        if (!empty($type)) {
            $qb->andWhere('t.type = ?3')
                ->setParameter(3, $type);
        }

        $tasks = $qb->getQuery()->getResult();

        return $tasks;
    }

    /**
     * Returns count of entities
     */
    public function getCount(?string $lastModified, ?string $type): int {
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(t.id)')
            ->from('Task', 't');
        
        if (!empty($lastModified)) {
            $qb->andWhere('t.modified >= ?3')
                ->setParameter(3, $lastModified);
        }
        if (!empty($type)) {
            $qb->andWhere('t.type = ?4')
                ->setParameter(4, $type);
        }

        $count = $qb->getQuery()->getSingleScalarResult();

        return (int) $count;
    }
}
