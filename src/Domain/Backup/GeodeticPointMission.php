<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * GeodeticPointMission
 *
 * @ORM\Table(name="geodetic_point_missions", indexes={@ORM\Index(name="geodetic_point_id", columns={"geodetic_point_id"}), @ORM\Index(name="group_id", columns={"group_id"}), @ORM\Index(name="mission_id", columns={"mission_id"})})
 * @ORM\Entity
 */
class GeodeticPointMission implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="mission_id", type="integer", nullable=false)
     */
    private $missionId;

    /**
     * @var int
     *
     * @ORM\Column(name="geodetic_point_id", type="integer", nullable=false)
     */
    private $geodeticPointId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $modified = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="group_id", type="bigint", nullable=true)
     */
    private $groupId;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \GeodeticPoint
     *
     * @ORM\OneToOne(targetEntity="GeodeticPoint")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="geodetic_point_id", referencedColumnName="id", unique=true)
     * })
     */
    private $geodeticPoint;

    /**
     * @var \Mission
     *
     * @ORM\OneToOne(targetEntity="Mission")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mission_id", referencedColumnName="id", unique=true)
     * })
     */
    private $mission;


    /**
     * Set missionId.
     *
     * @param int $missionId
     *
     * @return GeodeticPointMission
     */
    public function setMissionId($missionId)
    {
        $this->missionId = $missionId;

        return $this;
    }

    /**
     * Get missionId.
     *
     * @return int
     */
    public function getMissionId()
    {
        return $this->missionId;
    }

    /**
     * Set geodeticPointId.
     *
     * @param int $geodeticPointId
     *
     * @return GeodeticPointMission
     */
    public function setGeodeticPointId($geodeticPointId)
    {
        $this->geodeticPointId = $geodeticPointId;

        return $this;
    }

    /**
     * Get geodeticPointId.
     *
     * @return int
     */
    public function getGeodeticPointId()
    {
        return $this->geodeticPointId;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return GeodeticPointMission
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return GeodeticPointMission
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return GeodeticPointMission
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified.
     *
     * @param \DateTime $modified
     *
     * @return GeodeticPointMission
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified.
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set groupId.
     *
     * @param int|null $groupId
     *
     * @return GeodeticPointMission
     */
    public function setGroupId($groupId = null)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId.
     *
     * @return int|null
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set geodeticPoint.
     *
     * @param \GeodeticPoint|null $geodeticPoint
     *
     * @return GeodeticPointMission
     */
    public function setGeodeticPoint(\GeodeticPoint $geodeticPoint = null)
    {
        $this->geodeticPoint = $geodeticPoint;

        return $this;
    }

    /**
     * Get geodeticPoint.
     *
     * @return \GeodeticPoint|null
     */
    public function getGeodeticPoint()
    {
        return $this->geodeticPoint;
    }

    /**
     * Set mission.
     *
     * @param \Mission|null $mission
     *
     * @return GeodeticPointMission
     */
    public function setMission(\Mission $mission = null)
    {
        $this->mission = $mission;

        return $this;
    }

    /**
     * Get mission.
     *
     * @return \Mission|null
     */
    public function getMission()
    {
        return $this->mission;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'missionId' => $this->missionId,
            'geodeticPointId' => $this->geodeticPointId,
            'userId' => $this->userId,
            'groupId' => $this->groupId,
            'status' => $this->status,
            'created' => $this->created,
            'modified' => $this->modified,
            'geodeticPoint' => $this->geodeticPoint,
            'mission' => $this->mission,
        ];
    }
}
