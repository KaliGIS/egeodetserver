<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table(name="images", indexes={@ORM\Index(name="mission_id", columns={"mission_id"}), @ORM\Index(name="geodetic_point_id", columns={"geodetic_point_id"})})
 * @ORM\Entity
 */
class Image implements JsonSerializable
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="path", type="text", length=65535, nullable=true)
     */
    private $path;

    /**
     * @var int
     *
     * @ORM\Column(name="geodetic_point_id", type="integer", nullable=false)
     */
    private $geodeticPointId;

    /**
     * @var int
     *
     * @ORM\Column(name="mission_id", type="integer", nullable=false)
     */
    private $missionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $modified = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Image
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path.
     *
     * @param string|null $path
     *
     * @return Image
     */
    public function setPath($path = null)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string|null
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set geodeticPointId.
     *
     * @param int $geodeticPointId
     *
     * @return Image
     */
    public function setGeodeticPointId($geodeticPointId)
    {
        $this->geodeticPointId = $geodeticPointId;

        return $this;
    }

    /**
     * Get geodeticPointId.
     *
     * @return int
     */
    public function getGeodeticPointId()
    {
        return $this->geodeticPointId;
    }

    /**
     * Set missionId.
     *
     * @param int $missionId
     *
     * @return Image
     */
    public function setMissionId($missionId)
    {
        $this->missionId = $missionId;

        return $this;
    }

    /**
     * Get missionId.
     *
     * @return int
     */
    public function getMissionId()
    {
        return $this->missionId;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Image
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified.
     *
     * @param \DateTime $modified
     *
     * @return Image
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified.
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'ma,e' => $this->name,
            'path' => $this->path,
            'geodeticPointId' => $this->geodeticPointId,
            'missionId' => $this->missionId,
            'created' => $this->created,
            'modified' => $this->modified,
        ];
    }
}
