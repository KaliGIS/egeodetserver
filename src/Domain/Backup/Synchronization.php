<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Synchronization
 *
 * @ORM\Table(name="synchronizations")
 * @ORM\Entity
 */
class Synchronization implements JsonSerializable
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="sync_key", type="text", length=65535, nullable=true)
     */
    private $syncKey;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $startDate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status", type="integer", nullable=true, options={"comment"="0 - synced, 1 - syncing, 2 - synced with error"})
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set syncKey.
     *
     * @param string|null $syncKey
     *
     * @return Synchronization
     */
    public function setSyncKey($syncKey = null)
    {
        $this->syncKey = $syncKey;

        return $this;
    }

    /**
     * Get syncKey.
     *
     * @return string|null
     */
    public function getSyncKey()
    {
        return $this->syncKey;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return Synchronization
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Synchronization
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime|null $endDate
     *
     * @return Synchronization
     */
    public function setEndDate($endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return Synchronization
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'sync_key' => $this->syncKey,
            'user_id' => $this->userId,
            'status' => $this->status,
            'start_date' => $this->startDate,
            'end_date' => $this->endDate,
        ];
    }
}
