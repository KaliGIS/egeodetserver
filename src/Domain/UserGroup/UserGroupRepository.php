<?php
declare(strict_types=1);

namespace App\Domain\UserGroup;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Exception;
use DateTime;
use App\Domain\Validator\DateTimeValidator;
use App\Domain\Validator\RoomToMariaDb;

class UserGroupRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return UserGroup[]
     */
    public function findAll(int $fromId, int $limit, string $lastModified, ?int $userId, ?array $groupIds): array
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('ug')
            ->from('UserGroup', 'ug')
            ->innerJoin('ug.user', 'u')
            ->innerJoin('ug.group', 'g')
            ->where('ug.id >= ?1 AND ug.modified >= ?2')
            ->orderBy('ug.id', 'ASC')
            ->setMaxResults($limit + 1)
            ->setParameter(1, $fromId)
            ->setParameter(2, $lastModified);
        
        if (!empty($userId)) {
            $qb->andWhere('ug.userId = ?3')
                ->setParameter(3, $userId);
        }

        if (!empty($groupIds)) {
            $qb->andWhere('ug.groupId IN (:groupIds)')
                ->setParameter('groupIds', $groupIds);
        }

        $userGroups = $qb->getQuery()->getResult();

        return $userGroups;
    }

    /**
     * Returns count of entities
     */
    public function getCount(?string $lastModified, ?int $userId, ?array $groupIds): int {
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(ug.id)')
            ->from('UserGroup', 'ug');
        
        if (!empty($lastModified)) {
            $qb->andWhere('ug.modified >= ?3')
                ->setParameter(3, $lastModified);
        }
        if (!empty($userId)) {
            $qb->andWhere('ug.userId = ?4')
                ->setParameter(4, $userId);
        }

        if (!empty($groupIds)) {
            $qb->andWhere('ug.groupId IN (:groupIds)')
                ->setParameter('groupIds', $groupIds);
        }

        $count = $qb->getQuery()->getSingleScalarResult();

        return (int) $count;
    }
}
