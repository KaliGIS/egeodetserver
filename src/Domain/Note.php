<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 *
 * @ORM\Table(name="notes", indexes={@ORM\Index(name="geodetic_point_id", columns={"geodetic_point_id"}), @ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="mission_id", columns={"mission_id"})})
 * @ORM\Entity
 */
class Note implements jsonSerializable
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="note", type="text", length=65535, nullable=true)
     */
    private $note;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="geodetic_point_id", type="integer", nullable=false)
     */
    private $geodeticPointId;

    /**
     * @var int
     *
     * @ORM\Column(name="mission_id", type="integer", nullable=false)
     */
    private $missionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $modified = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Mission
     *
     * @ORM\OneToOne(targetEntity="Mission")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mission_id", referencedColumnName="id", unique=true)
     * })
     */
    private $mission;

    /**
     * Set note.
     *
     * @param string|null $note
     *
     * @return Note
     */
    public function setNote($note = null)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note.
     *
     * @return string|null
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return Note
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set geodeticPointId.
     *
     * @param int $geodeticPointId
     *
     * @return Note
     */
    public function setGeodeticPointId($geodeticPointId)
    {
        $this->geodeticPointId = $geodeticPointId;

        return $this;
    }

    /**
     * Get geodeticPointId.
     *
     * @return int
     */
    public function getGeodeticPointId()
    {
        return $this->geodeticPointId;
    }

    /**
     * Set missionId.
     *
     * @param int $missionId
     *
     * @return Note
     */
    public function setMissionId($missionId)
    {
        $this->missionId = $missionId;

        return $this;
    }

    /**
     * Get missionId.
     *
     * @return int
     */
    public function getMissionId()
    {
        return $this->missionId;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Note
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified.
     *
     * @param \DateTime $modified
     *
     * @return Note
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified.
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mission.
     *
     * @param \Mission|null $mission
     *
     * @return GeodeticPointTask
     */
    public function setMission(\Mission $mission = null)
    {
        $this->mission = $mission;

        return $this;
    }

    /**
     * Get mission.
     *
     * @return \Mission|null
     */
    public function getMission()
    {
        return $this->mission;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'note' => $this->note,
            'userId' => $this->userId,
            'geodeticPointId' => $this->geodeticPointId,
            'missionId' => $this->missionId,
            'created' => $this->created,
            'modified' => $this->modified,
        ];
    }
}
