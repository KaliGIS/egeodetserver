<?php
declare(strict_types=1);

namespace App\Domain\User;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use App\Domain\Validator\DateTimeValidator;
use DateTime;

class UserRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    private $defaultUserData = array(
        "name" => null,
        "role" => null,
        "email" => null,
        "password" => '',
        "created" => null,
        "modified" => null
    );

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return User
     */
    public function synchronizeUser(array $userData) : \User {
        $this->logger->info("Changed data: " . json_encode($userData));
        $name = empty($userData["name"]) ? null : $userData["name"];
        $userEntity = $this->em->getRepository('User')->findOneBy(array("name" => $name));

        if (!empty($userEntity)) {
            // update existing user
            $userEntity = $this->updateUser((int) $userEntity->getId(), $userData, $userEntity, true);
        } else {
            // insert new user
            $userEntity = $this->insertUser($userData, true);
        }

        return $userEntity;
    }

    /**
     * @return User
     */
    public function insertUser(array $userData, bool $isPwdHashed = false): \User
    {
        $harmonizedData = harmonizeData($userData, $isPwdHashed);
        
        $userEntity = new \User();
        $userEntity->setName($harmonizedData["name"]);
        $userEntity->setRole($harmonizedData["role"]);
        $userEntity->setEmail($harmonizedData["email"]);
        $userEntity->setPassword($harmonizedData["password"]);
        $userEntity->setCreated($harmonizedData["created"]);
        $userEntity->setModified($harmonizedData["modified"]);

        $this->em->persist($userEntity);
        $this->em->flush();

        return $userEntity;
    }

    public function updateUser(int $id, array $userData, \User $userEntity = null, bool $isPwdHashed = false): \User {
        $harmonizedData = $this->harmonizeData($userData, $isPwdHashed);

        if (empty($userEntity)) {
            $userEntity = $this->em->getRepository('User')->findOneBy(array("id" => $id));
        }
        
        $userEntity->setName($harmonizedData["name"]);
        $userEntity->setRole($harmonizedData["role"]);
        $userEntity->setEmail($harmonizedData["email"]);
        $userEntity->setPassword($harmonizedData["password"]);
        $userEntity->setCreated($harmonizedData["created"]);
        $userEntity->setModified($harmonizedData["modified"]);

        $this->em->flush();

        return $userEntity;
    }

    /**
     * Transforms data to be insertable
     * @return array
     */
    public function harmonizeData(array $userData, bool $isPwdHashed = false): array {
        if (array_key_exists("created", $userData) && DateTimeValidator::isValid($userData["created"])) {
            $userData["created"] = new DateTime($userData["created"]);
        } else {
            unset($userData["created"]);
        }
        if (array_key_exists("modified", $userData) && DateTimeValidator::isValid($userData["modified"])) {
            $userData["modified"] = new DateTime($userData["modified"]);
        } else {
            unset($userData["modified"]);
        }
        if (array_key_exists("password", $userData)) {
            if (!$isPwdHashed) {
                $userData["password"] = hash('sha256', $userData["password"]);
            }
        } else {
            unset($userData["password"]);
        }

        $harmonizedData = array_replace($this->defaultUserData, $userData);
        return $harmonizedData;
    }

    /**
     * @return User[]
     */
    public function findAll(int $fromId, int $limit): array
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('u')
            ->from('User', 'u')
            ->where('u.id >= ?1')
            ->orderBy('u.id', 'ASC')
            ->setMaxResults($limit + 1)
            ->setParameter(1, $fromId);
        $users = $qb->getQuery()->getResult();

        return $users;
    }

    public function getCount(): int {
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(u.id)')
            ->from('User', 'u');
        $count = $qb->getQuery()->getSingleScalarResult();

        return (int) $count;
    }

    public function findAdmins(): array {
        $qb = $this->em->createQueryBuilder();
        $qb->select('u')
            ->from('User', 'u')
            ->where('u.role = ?1')
            ->orderBy('u.id', 'ASC')
            ->setParameter(1, 'admin');
        $admins = $qb->getQuery()->getResult();

        return $admins;
    }
}
