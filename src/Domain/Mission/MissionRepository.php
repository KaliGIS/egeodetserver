<?php
declare(strict_types=1);

namespace App\Domain\Mission;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class MissionRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    private $defaultMission = array(
        "name" => null,
        "year" => null,
        "groupId" => null,
        "created" => null,
        "modified" => null
    );

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return Mission[]
     */
    public function findAll(int $fromId, int $limit, string $lastModified): array
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('m')
            ->from('Mission', 'm')
            ->where('m.id >= ?1 AND m.modified >= ?2')
            ->orderBy('m.id', 'ASC')
            ->setMaxResults($limit + 1)
            ->setParameter(1, $fromId)
            ->setParameter(2, $lastModified);
        
        $missions = $qb->getQuery()->getResult();

        return $missions;
    }

    /**
     * Returns count of entities
     */
    public function getCount(?string $lastModified): int {
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(m.id)')
            ->from('Mission', 'm');
        
        if (!empty($lastModified)) {
            $qb->andWhere('m.modified >= ?1')
                ->setParameter(1, $lastModified);
        }

        $count = $qb->getQuery()->getSingleScalarResult();

        return (int) $count;
    }
}
