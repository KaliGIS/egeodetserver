<?php
declare(strict_types=1);

namespace App\Domain\Synchronization;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use DateTime;
use App\Domain\Validator\DateTimeValidator;
use App\Domain\Validator\RoomToMariaDb;

class SynchronizationRepository
{
    private $EPOCH_START_DATE_TIME = "1970-01-01 00:00:00";

    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    private $defaultSynchronizationData = array(
        "id" => null,
        "syncKey" => null,
        "userId" => null,
        "status" => null,
        "startDate" => null,
        "endDate" => null,
    );

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return Synchronization
     */
    public function synchronizeSynchronization(array $synchronizationData): ?\Synchronization
    {        
        $syncKey = empty($synchronizationData["syncKey"]) ? null : $synchronizationData["syncKey"];
        $this->logger->info('SynchronizationData: ' . json_encode($synchronizationData));
        $synchronizationEntity = $this->em->getRepository('Synchronization')->findOneBy(array("syncKey" => $syncKey));

        if (!empty($synchronizationEntity)) {
            // update existing synchronization
            $synchronizationEntity = $this->updateSynchronization((string) $synchronizationEntity->getSyncKey(), $synchronizationData, $synchronizationEntity);
        } else if (empty($synchronizationEntity) && $syncKey != null) {
            // insert new synchronization
            $synchronizationEntity = $this->insertSynchronization($synchronizationData);
        }

        return $synchronizationEntity;
    }

    /**
     * Updates existing synchronization entity
     */
    public function updateSynchronization(string $syncKey, array $synchronizationData, \Synchronization $synchronizationEntity = null): \Synchronization {
        $harmonizedData = $this->harmonizeData($synchronizationData);
        
        if (empty($synchronizationEntity)) {
            $synchronizationEntity = $this->em->getRepository('Synchronization')->findOneBy(array("syncKey" => $syncKey));
        }
        
        $synchronizationEntity->setUserId($harmonizedData["userId"]);
        $synchronizationEntity->setStatus($harmonizedData["status"]);
        if (!empty($harmonizedData["startDate"])) {
            $this->logger->info('Start date is not empty');
            $synchronizationEntity->setStartDate($harmonizedData["startDate"]);  
        }
        $synchronizationEntity->setEndDate($harmonizedData["endDate"]);

        $this->logger->info('Synchronization entity to flush: ' . json_encode($synchronizationEntity));
        $this->em->flush();

        return $synchronizationEntity;
    }

    public function insertSynchronization(array $synchronizationData): \Synchronization {
        $harmonizedData = $this->harmonizeData($synchronizationData);

        $synchronizationEntity = new \Synchronization();

        $synchronizationEntity->setSyncKey($harmonizedData["syncKey"]);
        $synchronizationEntity->setUserId($harmonizedData["userId"]);
        $synchronizationEntity->setStatus($harmonizedData["status"]);
        $synchronizationEntity->setStartDate($harmonizedData["startDate"]);  
        $synchronizationEntity->setEndDate($harmonizedData["endDate"]);

        $this->em->persist($synchronizationEntity);
        $this->em->flush();

        return $synchronizationEntity;
    }

    /**
     * Transforms data to be insertable
     */
    public function harmonizeData(array $synchronizationData): array {
        $this->logger->info('EPOCH_START_DATE_TIME: ' . $this->EPOCH_START_DATE_TIME);
        if (array_key_exists("startDate", $synchronizationData) && $synchronizationData["startDate"] == $this->EPOCH_START_DATE_TIME) {
            $this->logger->info('Start date is epoch start date');
            $synchronizationData["startDate"] = new DateTime();
        } else if (array_key_exists("startDate", $synchronizationData) && DateTimeValidator::isValid($synchronizationData["startDate"])) {
            $this->logger->info('Start date is ' . $synchronizationData["startDate"]);
            $synchronizationData["startDate"] = new DateTime($synchronizationData["startDate"]);
        } else {
            unset($synchronizationData["startDate"]);
        }
        if (array_key_exists("endDate", $synchronizationData) && $synchronizationData["endDate"] == $this->EPOCH_START_DATE_TIME) {
            $this->logger->info('End date is epoch end date');
            $synchronizationData["endDate"] = new DateTime();
        } else if (array_key_exists("startDate", $synchronizationData) && DateTimeValidator::isValid($synchronizationData["endDate"])) {
            $this->logger->info('End date is ' . $synchronizationData["endDate"]);
            $synchronizationData["endDate"] = new DateTime($synchronizationData["endDate"]);
        } else {
            unset($synchronizationData["endDate"]);
        }

        $harmonizedData = array_replace($this->defaultSynchronizationData, $synchronizationData);
        return $harmonizedData;
    }

    /**
     * @return Synchronization[]
     */
    public function findAll(): array
    {
        $synchronizations = $this->em->getRepository('Synchronization')->findAll();

        return $synchronizations;
    }

    public function findById(int $id): \Synchronization {
        $synchronization = $this->em->getRepository('Synchronization')->findOneBy(array('id' => $id));
        $this->logger->info("Synchronization found by id: " . json_encode($synchronization));

        return $synchronization;
    }
}
