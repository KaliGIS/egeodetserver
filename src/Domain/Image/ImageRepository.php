<?php
declare(strict_types=1);

namespace App\Domain\Image;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use DateTime;
use App\Domain\Validator\DateTimeValidator;

class ImageRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    private $logger;

    private $defaultImageData = array(
        "name" => null,
        "path" => null,
        "geodeticPointId" => null,
        "missionId" => null,
        "created" => null,
        "modified" => null
    );

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return Image
     */
    public function synchronizeImage(array $imageData) : \Image {
        $name = empty($imageData["name"]) ? null : $imageData["name"];
        $path = empty($imageData["path"]) ? null : $imageData["path"];
        $imageEntity = $this->em->getRepository('Image')->findOneBy(array("name" => $name, "path" => $path));

        if (!empty($imageEntity)) {
            // update existing point
            $imageEntity = $this->updateImage((int) $imageEntity->getId(), $imageData, $imageEntity);
        } else {
            // insert new point
            $imageEntity = $this->insertImage($imageData);
        }

        return $imageEntity;
    }

    /**
     * @return Image
     */
    public function updateImage(int $id, array $imageData, \Image $imageEntity = null): \Image
    {
        $harmonizedData = $this->harmonizeData($imageData);
        
        if (empty($geodeticPointEntity)) {
            $imageEntity = $this->em->getRepository('Image')->findOneBy(array("id" => $id));
        }

        $imageEntity->setName($harmonizedData["name"]);
        $imageEntity->setPath($harmonizedData["path"]);
        $imageEntity->setGeodeticPointId($harmonizedData["geodeticPointId"]);
        $imageEntity->setMissionId($harmonizedData["missionId"]);
        $imageEntity->setCreated($harmonizedData["created"]);
        $imageEntity->setModified($harmonizedData["modified"]);

        $this->em->flush();

        return $imageEntity;
    }

    /**
     * @return Image
     */
    public function insertImage(array $imageData): \Image
    {
        $harmonizedData = $this->harmonizeData($imageData);
        
        $missionEntity = $this->em->getRepository('Mission')->findOneBy(array("id" => $harmonizedData["missionId"]));
        if (empty($missionEntity)) {
            throw new Exception("Mission entity does not exist for id " . $missionId);
        }
        
        $newImage = new \Image();
        $newImage->setName($harmonizedData["name"]);
        $newImage->setPath($harmonizedData["path"]);
        $newImage->setGeodeticPointId($harmonizedData["geodeticPointId"]);
        $newImage->setMissionId($harmonizedData["missionId"]);
        $newImage->setCreated($harmonizedData["created"]);
        $newImage->setModified($harmonizedData["modified"]);
        $newImage->setMission($missionEntity);

        $this->em->persist($newImage);
        $this->em->flush();

        return $newImage;
    }

    /**
     * Transforms data to be insertable
     */
    public function harmonizeData(array $imageData): array {
        if (array_key_exists("created", $imageData) && DateTimeValidator::isValid($imageData["created"])) {
            $imageData["created"] = new DateTime($imageData["created"]);
        } else {
            unset($imageData["created"]);
        }
        if (array_key_exists("modified", $imageData) && DateTimeValidator::isValid($imageData["modified"])) {
            $imageData["modified"] = new DateTime($imageData["modified"]);
        } else {
            unset($imageData["modified"]);
        }

        $harmonizedData = array_replace($this->defaultImageData, $imageData);
        return $harmonizedData;
    }

    /**
     * @return Image[]
     */
    public function findAll(int $fromId, int $limit, string $lastModified, array $missionIds = [], ?int $lifecyclePhase): array
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('i')
            ->from('Image', 'i')
            ->innerJoin('i.mission', 'm')
            ->where('i.id >= ?1 AND i.modified >= ?2')
            ->orderBy('i.id', 'ASC')
            ->setMaxResults($limit + 1)
            ->setParameter(1, $fromId)
            ->setParameter(2, $lastModified);
        
        if (count($missionIds) > 0) {
            $this->logger->info('Setting WHERE condition for mission ids: ' . json_encode($missionIds));
            $qb->andWhere('i.missionId IN (:missionIds)')
                ->setParameter('missionIds', $missionIds);
        }

        if (isset($lifecyclePhase)) {
            $qb->andWhere('m.lifecyclePhase = ?3')
                ->setParameter(3, $lifecyclePhase);
        }
        
        $images = $qb->getQuery()->getResult();

        return $images;
    }

    /**
     * Returns count of entities
     */
    public function getCount(?string $lastModified, array $missionIds = [], ?int $lifecyclePhase): int {
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(i.id)')
            ->from('Image', 'i')
            ->innerJoin('i.mission', 'm');
        
        if (!empty($lastModified)) {
            $qb->andWhere('i.modified >= ?1')
                ->setParameter(1, $lastModified);
        }
        if (count($missionIds) > 0) {
            $qb->andWhere('i.missionId IN (:missionIds)')
                ->setParameter('missionIds', $missionIds);
        }

        if (isset($lifecyclePhase)) {
            $qb->andWhere('m.lifecyclePhase = ?2')
                ->setParameter(2, $lifecyclePhase);
        }

        $count = $qb->getQuery()->getSingleScalarResult();

        return (int) $count;
    }
}
