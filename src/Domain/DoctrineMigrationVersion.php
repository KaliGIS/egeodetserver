<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * DoctrineMigrationVersion
 *
 * @ORM\Table(name="doctrine_migration_versions")
 * @ORM\Entity
 */
class DoctrineMigrationVersion
{
    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="executed_at", type="datetime", nullable=true)
     */
    private $executedAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="execution_time", type="integer", nullable=true)
     */
    private $executionTime;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=1024)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $version;


    /**
     * Set executedAt.
     *
     * @param \DateTime|null $executedAt
     *
     * @return DoctrineMigrationVersion
     */
    public function setExecutedAt($executedAt = null)
    {
        $this->executedAt = $executedAt;

        return $this;
    }

    /**
     * Get executedAt.
     *
     * @return \DateTime|null
     */
    public function getExecutedAt()
    {
        return $this->executedAt;
    }

    /**
     * Set executionTime.
     *
     * @param int|null $executionTime
     *
     * @return DoctrineMigrationVersion
     */
    public function setExecutionTime($executionTime = null)
    {
        $this->executionTime = $executionTime;

        return $this;
    }

    /**
     * Get executionTime.
     *
     * @return int|null
     */
    public function getExecutionTime()
    {
        return $this->executionTime;
    }

    /**
     * Get version.
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }
}
