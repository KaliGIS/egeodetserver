<?php
declare(strict_types=1);

namespace App\Application\Actions\GeodeticPointMission;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Validator\DateTimeValidator;

class ListGeodeticPointMissionsAction extends GeodeticPointMissionAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParams = $this->request->getQueryParams();
        $fromId = isset($queryParams['fromid']) ? $queryParams['fromid'] : 1;
        $limit = isset($queryParams['limit']) ? $queryParams['limit'] : 500;
        if ($limit > 500) {
            $limit = 500; // Maximum limit
        }
        $lastModified = isset($queryParams['lastmodified']) ? $queryParams['lastmodified'] : "1970-01-01 00:00:00";
        $lastModified = DateTimeValidator::isValid($lastModified) ? $lastModified : "1970-01-01 00:00:00";
        $userId = isset($queryParams['userid']) ? $queryParams['userid'] : null;
        $groupIds = $this->getGroupIdsFromParams($queryParams);
        $lifecyclePhase = isset($queryParams['lifecyclephase']) ? (int) $queryParams['lifecyclephase'] : null;

        $geodeticPointMissions = $this->geodeticPointMissionRepository->findAll((int) $fromId, (int) $limit, $lastModified, (int) $userId, $groupIds, $lifecyclePhase);
        $count = $this->geodeticPointMissionRepository->getCount($lastModified,(int) $userId, $groupIds, $lifecyclePhase);

        $responseData = array("count" => $count);

        if (count($geodeticPointMissions) > $limit) {
            $lastGeodeticPointMission = end($geodeticPointMissions);
            $responseData["geodeticPointMissions"] = array_slice($geodeticPointMissions, 0, count($geodeticPointMissions) - 1);
            $responseData["nextId"] = $lastGeodeticPointMission->getId();
        } else {
            $responseData["geodeticPointMissions"] = $geodeticPointMissions;
            $responseData["nextId"] = -1;
        }

        return $this->respondWithData($responseData);
    }

    /**
     * Returns pretty group ids array from various of inputs
     */
    protected function getGroupIdsFromParams(array $queryParams): array {
        $groupIdsRaw = isset($queryParams['groupids']) ? $queryParams['groupids'] : array();
        $groupIds = array();
        if (gettype($groupIdsRaw) == 'array') {
            foreach($groupIdsRaw as $groupId) {
                $groupIds[] = (int) $groupId;
            }
        } elseif (gettype($groupIdsRaw) == 'string') {
            $groupIds[] = $groupIdsRaw;
        }
        
        return $groupIds;
    }
}
