<?php
declare(strict_types=1);

namespace App\Application\Actions\GeodeticPointMission;

use App\Application\Actions\Action;
use App\Domain\GeodeticPointMission\GeodeticPointMissionRepository;
use Psr\Log\LoggerInterface;

abstract class GeodeticPointMissionAction extends Action
{
    /**
     * @var GeodeticPointMissionRepository
     */
    protected $geodeticPointMissionRepository;

    /**
     * @param LoggerInterface $logger
     * @param GeodeticPointMissionRepository  $geodeticPointMissionRepository
     */
    public function __construct(LoggerInterface $logger, GeodeticPointMissionRepository $geodeticPointMissionRepository)
    {
        parent::__construct($logger);
        $this->geodeticPointMissionRepository = $geodeticPointMissionRepository;
    }
}
