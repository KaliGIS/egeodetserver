<?php
declare(strict_types=1);

namespace App\Application\Actions\GeodeticPointMission;

use Psr\Http\Message\ResponseInterface as Response;

class SynchronizeGeodeticPointMissionAction extends GeodeticPointMissionAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();

        $synchronizedGeodeticPointMission = $this->geodeticPointMissionRepository->synchronizeGeodeticPointMission($body);

        return $this->respondWithData($synchronizedGeodeticPointMission);
    }
}
