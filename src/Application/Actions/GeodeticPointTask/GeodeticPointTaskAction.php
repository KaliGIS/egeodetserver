<?php
declare(strict_types=1);

namespace App\Application\Actions\GeodeticPointTask;

use App\Application\Actions\Action;
use App\Domain\GeodeticPointTask\GeodeticPointTaskRepository;
use App\Domain\GeodeticPointTaskConflict\GeodeticPointTaskConflictRepository;
use Psr\Log\LoggerInterface;

abstract class GeodeticPointTaskAction extends Action
{
    /**
     * @var GeodeticPointTaskRepository
     */
    protected $geodeticPointTaskRepository;

    /**
     * @var GeodeticPointTaskConflictRepository
     */
    protected $geodeticPointTaskConflictRepository;

    /**
     * @param LoggerInterface $logger
     * @param GeodeticPointTaskRepository  $geodeticPointTaskRepository
     */
    public function __construct(LoggerInterface $logger, GeodeticPointTaskRepository $geodeticPointTaskRepository, GeodeticPointTaskConflictRepository $geodeticPointTaskConflictRepository)
    {
        parent::__construct($logger);
        $this->geodeticPointTaskRepository = $geodeticPointTaskRepository;
        $this->geodeticPointTaskConflictRepository = $geodeticPointTaskConflictRepository;
    }
}
