<?php
declare(strict_types=1);

namespace App\Application\Actions\GeodeticPointTask;

use Psr\Http\Message\ResponseInterface as Response;

class InsertGeodeticPointTaskAction extends GeodeticPointTaskAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();
        $this->logger->info("Parsed body: " . json_encode($body));

        $newGeodeticPointTask = $this->geodeticPointTaskRepository->insertGeodeticPointTask($body);

        return $this->respondWithData($newGeodeticPointTask);
    }
}
