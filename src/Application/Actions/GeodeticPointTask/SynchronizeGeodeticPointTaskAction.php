<?php
declare(strict_types=1);

namespace App\Application\Actions\GeodeticPointTask;

use Psr\Http\Message\ResponseInterface as Response;

class SynchronizeGeodeticPointTaskAction extends GeodeticPointTaskAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();
        $wasConflict = false;
        
        $geodeticPointTaskEntity = $this->geodeticPointTaskRepository->recordExists($body);
        if (!empty($geodeticPointTaskEntity)) {
            $isUserIdSame = $this->geodeticPointTaskRepository->isUserIdSame($body, $geodeticPointTaskEntity);
            if ($isUserIdSame) {
                // update
                $synchronizedData = $this->geodeticPointTaskRepository->updateGeodeticPointTask((int) $geodeticPointTaskEntity->getId(), $body, $geodeticPointTaskEntity);
            } else {
                // insert into geodetic_point_task_conflicts
                $this->geodeticPointTaskConflictRepository->synchronizeGeodeticPointTaskConflict($body);
                $synchronizedData = $geodeticPointTaskEntity;
                $wasConflict = true;
            }
        } else {
            // insert
            $synchronizedData = $this->geodeticPointTaskRepository->insertGeodeticPointTask($body);
        }

        $responseData = array(
            'synchronizedData' => $synchronizedData,
            'wasConflict' => $wasConflict
        );

        return $this->respondWithData($responseData);
    }
}
