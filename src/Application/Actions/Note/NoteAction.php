<?php
declare(strict_types=1);

namespace App\Application\Actions\Note;

use App\Application\Actions\Action;
use App\Domain\Note\NoteRepository;
use App\Domain\User\UserRepository;
use Psr\Log\LoggerInterface;

abstract class NoteAction extends Action
{
    /**
     * @var NoteRepository
     */
    protected $noteRepository;

    /**
     * @param LoggerInterface $logger
     * @param NoteRepository  $noteRepository
     */
    public function __construct(LoggerInterface $logger, NoteRepository $noteRepository, UserRepository $userRepository)
    {
        parent::__construct($logger);
        $this->noteRepository = $noteRepository;
        $this->userRepository = $userRepository;
    }
}
