<?php
declare(strict_types=1);

namespace App\Application\Actions\Note;

use Psr\Http\Message\ResponseInterface as Response;

class SynchronizeNoteAction extends NoteAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();
        // $this->logger->info("Parsed body: " . json_encode($body));

        $synchronizedNote = $this->noteRepository->synchronizeNote($body);

        return $this->respondWithData($synchronizedNote);
    }
}
