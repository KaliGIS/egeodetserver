<?php
declare(strict_types=1);

namespace App\Application\Actions\Note;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Validator\DateTimeValidator;

class ListNotesAction extends NoteAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParams = $this->request->getQueryParams();
        $fromId = isset($queryParams['fromid']) ? $queryParams['fromid'] : 1;
        $limit = isset($queryParams['limit']) ? $queryParams['limit'] : 500;

        if ($limit > 500) {
            $limit = 500; // Maximum limit
        }
        $lastModified = isset($queryParams['lastmodified']) ? $queryParams['lastmodified'] : "1970-01-01 00:00:00";
        $lastModified = DateTimeValidator::isValid($lastModified) ? $lastModified : "1970-01-01 00:00:00";
        $userIds = isset($queryParams['userid']) ? array($queryParams['userid']) : array();
        $alsoAdminNotes = isset($queryParams['alsoadmin']) ? $queryParams['alsoadmin'] : false;
        $lifecyclePhase = isset($queryParams['lifecyclephase']) ? (int) $queryParams['lifecyclephase'] : null;

        if ($alsoAdminNotes && count($userIds)) {
            $admins = $this->userRepository->findAdmins();
            foreach($admins as $admin) {
                if (!in_array($admin->getId(), $userIds, true)) {
                    array_push($userIds, $admin->getId());
                }
            }
        }

        $notes = $this->noteRepository->findAll((int) $fromId, (int) $limit, $lastModified, $userIds, $lifecyclePhase);
        $count = $this->noteRepository->getCount($lastModified, $userIds, $lifecyclePhase);
        
        $responseData = array("count" => $count);

        if (count($notes) > $limit) {
            $lastNote = end($notes);
            $responseData["notes"] = array_slice($notes, 0, count($notes) - 1);
            $responseData["nextId"] = $lastNote->getId();
        } else {
            $responseData["notes"] = $notes;
            $responseData["nextId"] = -1;
        }

        return $this->respondWithData($responseData);
    }
}
