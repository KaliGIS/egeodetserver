<?php
declare(strict_types=1);

namespace App\Application\Actions\Task;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Validator\DateTimeValidator;

class ListTasksAction extends TaskAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParams = $this->request->getQueryParams();
        $fromId = isset($queryParams['fromid']) ? $queryParams['fromid'] : 1;
        $limit = isset($queryParams['limit']) ? $queryParams['limit'] : 500;
        if ($limit > 500) {
            $limit = 500; // Maximum limit
        }
        $lastModified = isset($queryParams['lastmodified']) ? $queryParams['lastmodified'] : "1970-01-01 00:00:00";
        $lastModified = DateTimeValidator::isValid($lastModified) ? $lastModified : "1970-01-01 00:00:00";
        $type = isset($queryParams['type']) ? $queryParams['type'] : null;

        $tasks = $this->taskRepository->findAll((int) $fromId, (int) $limit, $type, $lastModified);
        $count = $this->taskRepository->getCount($lastModified, $type);

        $responseData = array("count" => $count);

        if (count($tasks) > $limit) {
            $lastTask = end($tasks);
            $responseData["tasks"] = array_slice($tasks, 0, count($tasks) - 1);
            $responseData["nextId"] = $lastTask->getId();
        } else {
            $responseData["tasks"] = $tasks;
            $responseData["nextId"] = -1;
        }

        return $this->respondWithData($responseData);
    }
}
