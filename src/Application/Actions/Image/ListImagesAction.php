<?php
declare(strict_types=1);

namespace App\Application\Actions\Image;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Validator\DateTimeValidator;

class ListImagesAction extends ImageAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParams = $this->request->getQueryParams();
        $fromId = isset($queryParams['fromid']) ? $queryParams['fromid'] : 1;
        $limit = isset($queryParams['limit']) ? $queryParams['limit'] : 500;
        if ($limit > 500) {
            $limit = 500; // Maximum limit
        }
        $lastModified = isset($queryParams['lastmodified']) ? $queryParams['lastmodified'] : "1970-01-01 00:00:00";
        $lastModified = DateTimeValidator::isValid($lastModified) ? $lastModified : "1970-01-01 00:00:00";
        $missionIds = $this->getMissionIdsFromParams($queryParams);
        $lifecyclePhase = isset($queryParams['lifecyclephase']) ? (int) $queryParams['lifecyclephase'] : null;

        $images = $this->imageRepository->findAll((int) $fromId, (int) $limit, $lastModified, $missionIds, $lifecyclePhase);
        $count = $this->imageRepository->getCount($lastModified, $missionIds, $lifecyclePhase);
        
        $responseData = array("count" => $count);

        if (count($images) > $limit) {
            $lastImage = end($images);
            $responseData["images"] = array_slice($images, 0, count($images) - 1);
            $responseData["nextId"] = $lastImage->getId();
        } else {
            $responseData["images"] = $images;
            $responseData["nextId"] = -1;
        }

        return $this->respondWithData($responseData);
    }

    /**
     * Returns pretty mission ids array from various input types
     */
    protected function getMissionIdsFromParams(array $queryParams): array {
        $missionIdsRaw = isset($queryParams['missionids']) ? $queryParams['missionids'] : array();
        $missionIds = array();
        if (gettype($missionIdsRaw) == 'array') {
            foreach($missionIdsRaw as $missionId) {
                $missionIds[] = (int) $missionId;
            }
        } elseif (gettype($missionIdsRaw) == 'string') {
            $missionIds[] = $missionIdsRaw;
        }
        
        return $missionIds;
    }
}
