<?php
declare(strict_types=1);

namespace App\Application\Actions\Image;

use App\Application\Actions\Action;
use App\Domain\Image\ImageRepository;
use Psr\Log\LoggerInterface;
use Codeliner\ArrayReader\ArrayReader;

abstract class ImageAction extends Action
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ArrayReader
     */
    protected $config;

    /**
     * @var ImageRepository
     */
    protected $imageRepository;


    /**
     * @param LoggerInterface $logger
     * @param ImageRepository  $imageRepository
     */
    public function __construct(LoggerInterface $logger, ArrayReader $config, ImageRepository $imageRepository)
    {
        parent::__construct($logger);
        $this->config = $config;
        $this->imageRepository = $imageRepository;
    }
}
