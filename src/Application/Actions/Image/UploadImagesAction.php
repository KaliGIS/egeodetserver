<?php

namespace App\Application\Actions\Image;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\UploadedFileInterface;
use App\Application\Actions\ActionError;

final class UploadImagesAction extends ImageAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $uploadedFiles = $this->request->getUploadedFiles();
        // $this->logger->info(json_encode($uploadedFiles));

        $body = $this->request->getParsedBody();
        $imageData = $this->makeImageArrayFromParsedBody($body);
        // $this->logger->info("Image entity: " . json_encode($imageData));

        if (array_key_exists("path", $imageData) && !empty($imageData["path"])) {
            $imageData["path"] = $this->harmonizePath($imageData["path"]);
            $destPath = $this->config->stringValue('uploadDirectory') . DIRECTORY_SEPARATOR . $imageData["path"];
        } else {
            return $this->respondWithError("Request parameter 'path' is not available", ActionError::RESOURCE_NOT_FOUND, 404);
        }

        if (array_key_exists("images", $uploadedFiles)) {
            foreach ($uploadedFiles['images'] as $uploadedFile) {
                if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                    $filename = $this->moveUploadedFile($destPath, $uploadedFile);
                }
                break; // Process only the first image. More images cause the method misused.
            }
        }

        $newImage = $this->imageRepository->synchronizeImage($imageData);

        return $this->respondWithData($newImage);
    }

    /**
     * Makes image associative array from parsed body
     */
    function makeImageArrayFromParsedBody(array $body): array {
        $imageData = array();
        foreach(array_keys($body) as $key) {
            $imageData[$key] = $body[$key];    
        }

        return $imageData;
    }

    /**
     * Moves the uploaded file to the upload directory and assigns it a unique name
     * to avoid overwriting an existing uploaded file.
     *
     * @param string $destPath The directory to which the file is moved
     * @param UploadedFileInterface $uploadedFile The file uploaded file to move
     *
     * @return string The filename of moved file
     */
    function moveUploadedFile(string $destPath, UploadedFileInterface $uploadedFile)
    {
        if (!file_exists($destPath)) {
            mkdir($destPath, 0777, true);
        }
        
        $filename = pathinfo($uploadedFile->getClientFilename(), PATHINFO_FILENAME);
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $this->logger->info("Processing uploaded file basename: " . $filename);
        $this->logger->info("Processing uploaded file extension: " . $extension);
        $filename = sprintf("%s.%s", $filename, $extension);

        $uploadedFile->moveTo($destPath . DIRECTORY_SEPARATOR . $filename);

        return $filename;
    }

    function harmonizePath(string $path): string {
        $reversedPath = strrev($path);
        $harmonizedReversedPath = $reversedPath[0];
        $prevChar = $reversedPath[0];
        for ($i = 1; $i < strlen($reversedPath); $i++) {
            if ($prevChar == '/' && $reversedPath[$i] == '.') { // Ommit the dot (.)
                $prevChar = $reversedPath[$i];
            } else {
                $harmonizedReversedPath .= $reversedPath[$i];
                $prevChar = $reversedPath[$i];
            }
        }

        return strrev($harmonizedReversedPath);
    }
}
