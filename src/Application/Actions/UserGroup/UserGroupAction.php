<?php
declare(strict_types=1);

namespace App\Application\Actions\UserGroup;

use App\Application\Actions\Action;
use App\Domain\UserGroup\UserGroupRepository;
use Psr\Log\LoggerInterface;

abstract class UserGroupAction extends Action
{
    /**
     * @var UserGroupRepository
     */
    protected $userGroupRepository;

    /**
     * @param LoggerInterface $logger
     * @param UserGroupRepository  $userGroupRepository
     */
    public function __construct(LoggerInterface $logger, UserGroupRepository $userGroupRepository)
    {
        parent::__construct($logger);
        $this->userGroupRepository = $userGroupRepository;
    }
}
