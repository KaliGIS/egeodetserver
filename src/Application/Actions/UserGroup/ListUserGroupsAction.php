<?php
declare(strict_types=1);

namespace App\Application\Actions\UserGroup;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Validator\DateTimeValidator;

class ListUserGroupsAction extends UserGroupAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParams = $this->request->getQueryParams();
        $fromId = isset($queryParams['fromid']) ? $queryParams['fromid'] : 1;
        $limit = isset($queryParams['limit']) ? $queryParams['limit'] : 500;
        if ($limit > 500) {
            $limit = 500; // Maximum limit
        }
        $lastModified = isset($queryParams['lastmodified']) ? $queryParams['lastmodified'] : "1970-01-01 00:00:00";
        $lastModified = DateTimeValidator::isValid($lastModified) ? $lastModified : "1970-01-01 00:00:00";
        $userId = isset($queryParams['userid']) ? $queryParams['userid'] : null;
        $groupIds = $this->getGroupIdsFromParams($queryParams);

        $userGroups = $this->userGroupRepository->findAll((int) $fromId, (int) $limit, $lastModified, (int) $userId, $groupIds);
        $count = $this->userGroupRepository->getCount($lastModified, (int) $userId, $groupIds);
        
        $responseData = array("count" => $count);

        if (count($userGroups) > $limit) {
            $lastUserGroup = end($userGroups);
            $responseData["userGroups"] = array_slice($userGroups, 0, count($userGroups) - 1);
            $responseData["nextId"] = $lastUserGroup->getId();
        } else {
            $responseData["userGroups"] = $userGroups;
            $responseData["nextId"] = -1;
        }

        return $this->respondWithData($responseData);
    }

    /**
     * Returns pretty group ids array from various of inputs
     */
    protected function getGroupIdsFromParams(array $queryParams): array {
        $groupIdsRaw = isset($queryParams['groupids']) ? $queryParams['groupids'] : array();
        $groupIds = array();
        if (gettype($groupIdsRaw) == 'array') {
            foreach($groupIdsRaw as $groupId) {
                $groupIds[] = (int) $groupId;
            }
        } elseif (gettype($groupIdsRaw) == 'string') {
            $groupIds[] = $groupIdsRaw;
        }
        
        return $groupIds;
    }
}
