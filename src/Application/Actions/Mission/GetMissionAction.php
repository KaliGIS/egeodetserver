<?php
declare(strict_types=1);

namespace App\Application\Actions\Mission;

use Psr\Http\Message\ResponseInterface as Response;

class GetMissionAction extends MissionAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();
        $id = (int) $this->resolveArg('id');
        $mission = $this->missionRepository->findById($id);

        return $this->respondWithData($mission);
    }
}
