<?php
declare(strict_types=1);

namespace App\Application\Actions\Mission;

use App\Application\Actions\Action;
use App\Domain\Mission\MissionRepository;
use Psr\Log\LoggerInterface;

abstract class MissionAction extends Action
{
    /**
     * @var MissionRepository
     */
    protected $missionRepository;

    /**
     * @param LoggerInterface $logger
     * @param MissionRepository  $missionRepository
     */
    public function __construct(LoggerInterface $logger, MissionRepository $missionRepository)
    {
        parent::__construct($logger);
        $this->missionRepository = $missionRepository;
    }
}
