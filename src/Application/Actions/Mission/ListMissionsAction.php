<?php
declare(strict_types=1);

namespace App\Application\Actions\Mission;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Validator\DateTimeValidator;

class ListMissionsAction extends MissionAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParams = $this->request->getQueryParams();
        $fromId = isset($queryParams['fromid']) ? $queryParams['fromid'] : 1;
        $limit = isset($queryParams['limit']) ? $queryParams['limit'] : 500;
        if ($limit > 500) {
            $limit = 500; // Maximum limit
        }
        $lastModified = isset($queryParams['lastModified']) ? $queryParams['lastModified'] : "1970-01-01 00:00:00";
        $lastModified = DateTimeValidator::isValid($lastModified) ? $lastModified : "1970-01-01 00:00:00";

        $missions = $this->missionRepository->findAll((int) $fromId, (int) $limit, $lastModified);
        $count = $this->missionRepository->getCount($lastModified);

        $responseData = array("count" => $count);

        if (count($missions) > $limit) {
            $lastMission = end($missions);
            $responseData["missions"] = array_slice($missions, 0, count($missions) - 1);
            $responseData["nextId"] = $lastMission->getId();
        } else {
            $responseData["missions"] = $missions;
            $responseData["nextId"] = -1;
        }

        return $this->respondWithData($responseData);
    }
}
