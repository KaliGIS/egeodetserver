<?php
declare(strict_types=1);

namespace App\Application\Actions\Mission;

use Psr\Http\Message\ResponseInterface as Response;

class SynchronizeMissionAction extends MissionAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();
        // $this->logger->info("Parsed body: " . json_encode($body));

        $synchronizedMission = $this->missionRepository->synchronizeMission($body);

        return $this->respondWithData($synchronizedMission);
    }
}
