<?php
declare(strict_types=1);

namespace App\Application\Actions\Synchronization;

use Psr\Http\Message\ResponseInterface as Response;

class GetSynchronizationAction extends SynchronizationAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();
        $id = (int) $this->resolveArg('id');
        $synchronization = $this->synchronizationRepository->findById($id);

        return $this->respondWithData($synchronization);
    }
}
