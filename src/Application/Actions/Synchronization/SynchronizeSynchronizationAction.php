<?php
declare(strict_types=1);

namespace App\Application\Actions\Synchronization;

use Psr\Http\Message\ResponseInterface as Response;

class SynchronizeSynchronizationAction extends SynchronizationAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();
        // $this->logger->info("Parsed body: " . json_encode($body));

        $synchronizedSynchronization = $this->synchronizationRepository->synchronizeSynchronization($body);

        return $this->respondWithData($synchronizedSynchronization);
    }
}
