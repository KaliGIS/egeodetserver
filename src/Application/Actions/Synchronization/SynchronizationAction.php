<?php
declare(strict_types=1);

namespace App\Application\Actions\Synchronization;

use App\Application\Actions\Action;
use App\Domain\Synchronization\SynchronizationRepository;
use Psr\Log\LoggerInterface;

abstract class SynchronizationAction extends Action
{
    /**
     * @var SynchronizationRepository
     */
    protected $synchronizationRepository;

    /**
     * @param LoggerInterface $logger
     * @param SynchronizationRepository  $synchronizationRepository
     */
    public function __construct(LoggerInterface $logger, SynchronizationRepository $synchronizationRepository)
    {
        parent::__construct($logger);
        $this->synchronizationRepository = $synchronizationRepository;
    }
}
