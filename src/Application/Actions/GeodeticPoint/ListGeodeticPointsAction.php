<?php
declare(strict_types=1);

namespace App\Application\Actions\GeodeticPoint;

use Psr\Http\Message\ResponseInterface as Response;

class ListGeodeticPointsAction extends GeodeticPointAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $geodeticPoints = $this->geodeticPointRepository->findAll();

        // $this->logger->info("Geodetic points list was viewed.");


        return $this->respondWithData($geodeticPoints);
    }
}
