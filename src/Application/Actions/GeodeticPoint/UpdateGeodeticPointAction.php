<?php
declare(strict_types=1);

namespace App\Application\Actions\GeodeticPoint;

use Psr\Http\Message\ResponseInterface as Response;

class UpdateGeodeticPointAction extends GeodeticPointAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();
        $id = (int) $this->resolveArg('id');
        // $this->logger->info("Parsed body: " . json_encode($body));

        $updatedGeodeticPoint = $this->geodeticPointRepository->updateGeodeticPoint($id, $body);

        return $this->respondWithData($updatedGeodeticPoint);
    }
}
