<?php
declare(strict_types=1);

namespace App\Application\Actions\GeodeticPoint;

use App\Application\Actions\Action;
use App\Domain\GeodeticPoint\GeodeticPointRepository;
use Psr\Log\LoggerInterface;

abstract class GeodeticPointAction extends Action
{
    /**
     * @var GeodeticPointRepository
     */
    protected $geodeticPointRepository;

    /**
     * @param LoggerInterface $logger
     * @param GeodeticPointRepository  $geodeticPointRepository
     */
    public function __construct(LoggerInterface $logger, GeodeticPointRepository $geodeticPointRepository)
    {
        parent::__construct($logger);
        $this->geodeticPointRepository = $geodeticPointRepository;
    }
}
