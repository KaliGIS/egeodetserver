<?php
declare(strict_types=1);

namespace App\Application\Actions\GeodeticPoint;

use Psr\Http\Message\ResponseInterface as Response;

class InsertGeodeticPointAction extends GeodeticPointAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();
        // $this->logger->info("Parsed body: " . json_encode($body));

        $newGeodeticPoint = $this->geodeticPointRepository->insertGeodeticPoint($body);

        return $this->respondWithData($newGeodeticPoint);
    }
}
