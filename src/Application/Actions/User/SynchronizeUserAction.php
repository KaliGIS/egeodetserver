<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;

class SynchronizeUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $body = $this->request->getParsedBody();
        // $this->logger->info("Parsed body: " . json_encode($body));

        $synchronizedUser = $this->userRepository->synchronizeUser($body);

        return $this->respondWithData($synchronizedUser);
    }
}
