<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;

class ListUsersAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParams = $this->request->getQueryParams();
        $fromId = isset($queryParams['fromid']) ? $queryParams['fromid'] : 1;
        $limit = isset($queryParams['limit']) ? $queryParams['limit'] : 500;
        if ($limit > 500) {
            $limit = 500; // Maximum limit
        }

        $users = $this->userRepository->findAll((int) $fromId, (int) $limit);
        $count = $this->userRepository->getCount();

        $responseData = array("count" => $count);

        if (count($users) > $limit) {
            $lastUser = end($users);
            $responseData["users"] = array_slice($users, 0, count($users) - 1);
            $responseData["nextId"] = $lastUser->getId();
        } else {
            $responseData["users"] = $users;
            $responseData["nextId"] = -1;
        }

        return $this->respondWithData($responseData);
    }
}
