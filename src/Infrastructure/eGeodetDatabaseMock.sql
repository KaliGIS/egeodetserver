CALL selectOrInsertMission('4531_1cast', '2020', NULL, @last_mission_id);

-- Ján Bublavý data 
CALL selectOrInsertUser('Ján Bublavý', 'geodet', NULL, 'Jano', @jan_bublavy_user_id);

-- Andrej Gladiš data
CALL selectOrInsertUser('Andrej Gladiš', 'geodet', 'andrej.gladis@skgeodesy.sk', 'Andrej', @andrej_gladis_user_id);

-- Create group SkJanoAndrej and assign users
CALL selectOrInsertGroup('SkJanoAndrej', @group_janoandrej_id);
INSERT INTO `user_groups` (`user_id`, `group_id`) VALUES (@jan_bublavy_user_id, @group_janoandrej_id);
INSERT INTO `user_groups` (`user_id`, `group_id`) VALUES (@andrej_gladis_user_id, @group_janoandrej_id);

CALL selectOrInsertGeodeticPoint('ZR16-505', 'ŠNS', NULL, @last_geodetic_point_id);
INSERT INTO `geodetic_point_missions` (`mission_id`, `geodetic_point_id`, `group_id`, `status`) VALUES (@last_mission_id, @last_geodetic_point_id, @group_janoandrej_id, '0'); 
CALL selectOrInsertTask('Nový bod', 'geodetický bod', @last_task_id);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Zachovalý', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Zničený', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Nenájdený', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Úprava terénu', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Vyvŕtanie dierky', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Náter GB', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Úprava miestopisu', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Geodetické meranie', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Osadenie novej OT', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Osadenie novej SK', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Náter OT', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Náter SK', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Osadenie OT do správnej polohy', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Osadenie SK do správnej polohy', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Úprava terénu okolo OT alebo SK', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Nálepka', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Fólia', 'iné');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('SKPOS', 'iné');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('SGRN', 'iné');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_janoandrej_id, 0, 0, NULL);

-- Another example of using variables
SELECT @novy_bod_task_id := `id` FROM `tasks` WHERE `name` = "Nový bod" LIMIT 1;
SELECT @zachovaly_task_id := `id` FROM `tasks` WHERE `name` = "Zachovalý" LIMIT 1; 
SELECT @zniceny_task_id := `id` FROM `tasks` WHERE `name` = "Zničený" LIMIT 1; 
SELECT @nenajdeny_task_id := `id` FROM `tasks` WHERE `name` = "Nenájdený" LIMIT 1; 
SELECT @uprava_terenu_task_id := `id` FROM `tasks` WHERE `name` = "Úprava terénu" LIMIT 1; 
SELECT @vyvrtanie_dierky_task_id := `id` FROM `tasks` WHERE `name` = "Vyvŕtanie dierky" LIMIT 1; 
SELECT @nater_gb_task_id := `id` FROM `tasks` WHERE `name` = "Náter GB" LIMIT 1; 
SELECT @uprava_miestopisu_task_id := `id` FROM `tasks` WHERE `name` = "Úprava miestopisu" LIMIT 1; 
SELECT @geodeticke_meranie_task_id := `id` FROM `tasks` WHERE `name` = "Geodetické meranie" LIMIT 1; 
SELECT @osadenie_novej_ot_task_id := `id` FROM `tasks` WHERE `name` = "Osadenie novej OT" LIMIT 1; 
SELECT @osadenie_novej_sk_task_id := `id` FROM `tasks` WHERE `name` = "Osadenie novej SK" LIMIT 1; 
SELECT @nater_ot_task_id := `id` FROM `tasks` WHERE `name` = "Náter OT" LIMIT 1; 
SELECT @nater_sk_task_id := `id` FROM `tasks` WHERE `name` = "Náter SK" LIMIT 1; 
SELECT @osadenie_ot_do_spravnej_polohy_task_id := `id` FROM `tasks` WHERE `name` = "Osadenie OT do správnej polohy" LIMIT 1; 
SELECT @osadenie_sk_do_spravnej_polohy_task_id := `id` FROM `tasks` WHERE `name` = "Osadenie SK do správnej polohy" LIMIT 1; 
SELECT @uprava_terenu_okolo_ot_alebo_sk_task_id := `id` FROM `tasks` WHERE `name` = "Úprava terénu okolo OT alebo SK" LIMIT 1; 
SELECT @nalepka_task_id := `id` FROM `tasks` WHERE `name` = "Nálepka" LIMIT 1; 
SELECT @folia_task_id := `id` FROM `tasks` WHERE `name` = "Fólia" LIMIT 1; 
SELECT @skpos_task_id := `id` FROM `tasks` WHERE `name` = "SKPOS" LIMIT 1; 
SELECT @sgrn_task_id := `id` FROM `tasks` WHERE `name` = "SGRN" LIMIT 1; 

CALL selectOrInsertGeodeticPoint('ZR16-506', 'ŠNS', NULL, @last_geodetic_point_id);
INSERT INTO `geodetic_point_missions` (`mission_id`, `geodetic_point_id`, `group_id`, `status`) VALUES (@last_mission_id, @last_geodetic_point_id, @group_janoandrej_id, '0'); 
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @novy_bod_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zachovaly_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zniceny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nenajdeny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @vyvrtanie_dierky_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_gb_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_miestopisu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @geodeticke_meranie_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_ot_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_sk_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_okolo_ot_alebo_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nalepka_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @folia_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @skpos_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @sgrn_task_id, @group_janoandrej_id, 0, 0, NULL);

CALL selectOrInsertGeodeticPoint('ZR16-507', 'ŠNS', NULL, @last_geodetic_point_id);
INSERT INTO `geodetic_point_missions` (`mission_id`, `geodetic_point_id`, `group_id`, `status`) VALUES (@last_mission_id, @last_geodetic_point_id, @group_janoandrej_id, '0');
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @novy_bod_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zachovaly_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zniceny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nenajdeny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @vyvrtanie_dierky_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_gb_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_miestopisu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @geodeticke_meranie_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_ot_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_sk_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_okolo_ot_alebo_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nalepka_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @folia_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @skpos_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @sgrn_task_id, @group_janoandrej_id, 0, 0, NULL);

CALL selectOrInsertGeodeticPoint('ZR16-508', 'ŠNS', NULL, @last_geodetic_point_id);
INSERT INTO `geodetic_point_missions` (`mission_id`, `geodetic_point_id`, `group_id`, `status`) VALUES (@last_mission_id, @last_geodetic_point_id, @group_janoandrej_id, '0');
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @novy_bod_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zachovaly_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zniceny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nenajdeny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @vyvrtanie_dierky_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_gb_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_miestopisu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @geodeticke_meranie_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_ot_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_sk_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_okolo_ot_alebo_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nalepka_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @folia_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @skpos_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @sgrn_task_id, @group_janoandrej_id, 0, 0, NULL);

CALL selectOrInsertGeodeticPoint('ZR16-509', 'ŠNS', NULL, @last_geodetic_point_id);
INSERT INTO `geodetic_point_missions` (`mission_id`, `geodetic_point_id`, `group_id`, `status`) VALUES (@last_mission_id, @last_geodetic_point_id, @group_janoandrej_id, '0');
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @novy_bod_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zachovaly_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zniceny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nenajdeny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @vyvrtanie_dierky_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_gb_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_miestopisu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @geodeticke_meranie_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_ot_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_sk_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_okolo_ot_alebo_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nalepka_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @folia_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @skpos_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @sgrn_task_id, @group_janoandrej_id, 0, 0, NULL);

CALL selectOrInsertGeodeticPoint('ZR16-510', 'ŠNS', NULL, @last_geodetic_point_id);
INSERT INTO `geodetic_point_missions` (`mission_id`, `geodetic_point_id`, `group_id`, `status`) VALUES (@last_mission_id, @last_geodetic_point_id, @group_janoandrej_id, '0');
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @novy_bod_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zachovaly_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zniceny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nenajdeny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @vyvrtanie_dierky_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_gb_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_miestopisu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @geodeticke_meranie_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_ot_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_sk_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_okolo_ot_alebo_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nalepka_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @folia_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @skpos_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @sgrn_task_id, @group_janoandrej_id, 0, 0, NULL);

-- Geodetic examination tasks
INSERT INTO `tasks` (`name`, `type`) VALUES  ('RTN', 'Geodetické meranie');
INSERT INTO `tasks` (`name`, `type`) VALUES  ('S3', 'Geodetické meranie');
INSERT INTO `tasks` (`name`, `type`) VALUES  ('S2', 'Geodetické meranie');
INSERT INTO `tasks` (`name`, `type`) VALUES  ('S1', 'Geodetické meranie');
INSERT INTO `tasks` (`name`, `type`) VALUES  ('POLY', 'Geodetické meranie');
INSERT INTO `tasks` (`name`, `type`) VALUES  ('RAJ', 'Geodetické meranie');
INSERT INTO `tasks` (`name`, `type`) VALUES  ('ED-RTN', 'Geodetické meranie');
INSERT INTO `tasks` (`name`, `type`) VALUES  ('ED-S3', 'Geodetické meranie');
INSERT INTO `tasks` (`name`, `type`) VALUES  ('ED-S2', 'Geodetické meranie');

-- Border stone and protective rod tasks
INSERT INTO `tasks` (`name`, `type`) VALUES ('Oprava poškodeného HZ', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Nahradenie poškodeného HZ', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Doplnenie chýbajúceho HZ', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Narovnanie mierne nakloneného HZ', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Prestabilizácia HZ', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Premiestnennie HZ na bezpečné miesto', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Geodetické meranie', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Osadenie nového HZ', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Výmena alebo osadenie nového nádstavca', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Náter a popis HZ', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Odkopanie hraničného znaku', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Dosypanie hraničného znaku', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Nahradenie poškodenej PZ', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Nová PZ', 'hraničný znak');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Nová ochranná tyč', 'ochranná tyč');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Náter ochrannej tyče', 'ochranná tyč');
INSERT INTO `tasks` (`name`, `type`) VALUES ('Osadenie OT do správnej polohy', 'ochranná tyč');

-- Notes
SELECT @ZR16_505_geodetic_point_id := `id` FROM `geodetic_points` WHERE `label` = "ZR16-505" LIMIT 1;
INSERT INTO `notes` (`note`, `user_id`, `geodetic_point_id`, `mission_id`) VALUES ('M. NAKL.', @jan_bublavy_user_id, @ZR16_505_geodetic_point_id, @last_mission_id);
INSERT INTO `notes` (`note`, `user_id`, `geodetic_point_id`, `mission_id`) VALUES ('VYČNIEVAJÚCI.', @jan_bublavy_user_id, @ZR16_505_geodetic_point_id, @last_mission_id);

-- Mission 2
INSERT INTO `missions` (`name`, `year`) VALUES ('mission 2', '2021');
SET @last_mission_id = LAST_INSERT_ID();

CALL selectOrInsertGeodeticPoint('POINT 1', 'ŠNS', NULL, @last_geodetic_point_id);
INSERT INTO `geodetic_point_missions` (`mission_id`, `geodetic_point_id`, `group_id`, `status`) VALUES (@last_mission_id, @last_geodetic_point_id, @group_janoandrej_id, '0'); 
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @novy_bod_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zachovaly_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zniceny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nenajdeny_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @vyvrtanie_dierky_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_gb_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_miestopisu_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @geodeticke_meranie_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_ot_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_ot_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_sk_do_spravnej_polohy_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_okolo_ot_alebo_sk_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nalepka_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @folia_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @skpos_task_id, @group_janoandrej_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @sgrn_task_id, @group_janoandrej_id, 0, 0, NULL);

-- Mission 3
INSERT INTO `missions` (`name`, `year`) VALUES ('mission 3', '2021');
SET @last_mission_id = LAST_INSERT_ID();

CALL selectOrInsertUser('John Doe', 'geodet', 'john.doe@skgeodesy.sk', 'John', @john_doe_user_id);

-- Create group SkJohnDoe and assign user
CALL selectOrInsertGroup('SkJohnDoe', @group_johndoe_id);
INSERT INTO `user_groups` (`user_id`, `group_id`) VALUES (@john_doe_user_id, @group_johndoe_id);

CALL selectOrInsertGeodeticPoint('POINT 2', 'ŠNS', NULL, @last_geodetic_point_id);
INSERT INTO `geodetic_point_missions` (`mission_id`, `geodetic_point_id`, `group_id`, `status`) VALUES (@last_mission_id, @last_geodetic_point_id, @group_johndoe_id, '0'); 
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @novy_bod_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zachovaly_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @zniceny_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nenajdeny_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @vyvrtanie_dierky_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_gb_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_miestopisu_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @geodeticke_meranie_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_ot_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_novej_sk_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_ot_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nater_sk_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_ot_do_spravnej_polohy_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @osadenie_sk_do_spravnej_polohy_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @uprava_terenu_okolo_ot_alebo_sk_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @nalepka_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @folia_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @skpos_task_id, @group_johndoe_id, 0, 0, NULL);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @sgrn_task_id, @group_johndoe_id, 0, 0, NULL);

