SET @mission_name = '4422_Vinicne';
SET @mission_year = 2021;
SELECT gp.id AS 'Id bodu', m.name AS Misia, m.year AS Rok, gp.label AS ČB, gpm.status AS Status, 
gpm.modified AS 'Dátum poslednej úpravy', u.name AS Geodet,
(SELECT group_concat(note, '; ') FROM notes WHERE notes.geodetic_point_id = gp.id) AS Poznámka,
MAX(CASE WHEN t.name = 'Nový bod' THEN gpt.is_done END) AS 'Nový bod',
MAX(CASE WHEN t.name = 'Zachovalý' THEN gpt.is_done END) AS 'Zachovalý',
MAX(CASE WHEN t.name = 'Zničený' THEN gpt.is_done END) AS 'Zničený',
MAX(CASE WHEN t.name = 'Nenájdený' THEN gpt.is_done END) AS 'Nenájdený',
MAX(CASE WHEN t.name = 'Úprava terénu' THEN gpt.is_done END) AS 'Úprava terénu',
MAX(CASE WHEN t.name = 'Vyvŕtanie dierky' THEN gpt.is_done END) AS 'Vyvŕtanie dierky',
MAX(CASE WHEN t.name = 'Náter GB' THEN gpt.is_done END) AS 'Náter GB',
MAX(CASE WHEN t.name = 'Úprava miestopisu' THEN gpt.is_done END) AS 'Úprava miestopisu',
MAX(CASE WHEN t.name = 'Osadenie novej OT' THEN gpt.is_done END) AS 'Osadenie novej OT', 
MAX(CASE WHEN t.name = 'Osadenie novej SK' THEN gpt.is_done END) AS 'Osadenie novej SK', 
MAX(CASE WHEN t.name = 'Náter OT' THEN gpt.is_done END) AS 'Náter OT', 
MAX(CASE WHEN t.name = 'Náter SK' THEN gpt.is_done END) AS 'Náter SK', 
MAX(CASE WHEN t.name = 'Osadenie OT do správnej polohy' THEN gpt.is_done END) AS 'Osadenie OT do správnej polohy', 
MAX(CASE WHEN t.name = 'Osadenie SK do správnej polohy' THEN gpt.is_done END) AS 'Osadenie SK do správnej polohy', 
MAX(CASE WHEN t.name = 'Úprava terénu okolo OT alebo SK' THEN gpt.is_done END) AS 'Úprava terénu okolo OT alebo SK', 
MAX(CASE WHEN t.name = 'Nálepka' THEN gpt.is_done END) AS 'Nálepka', 
MAX(CASE WHEN t.name = 'Fólia' THEN gpt.is_done END) AS 'Fólia', 
MAX(CASE WHEN t.name = 'SKPOS' THEN gpt.is_done END) AS 'SKPOS', 
MAX(CASE WHEN t.name = 'SGRN' THEN gpt.is_done END) AS 'SGRN', 
MAX(CASE WHEN t.name = 'RTN' THEN gpt.is_done END) AS 'RTN', 
MAX(CASE WHEN t.name = 'S3' THEN gpt.is_done END) AS 'S3', 
MAX(CASE WHEN t.name = 'S2' THEN gpt.is_done END) AS 'S2', 
MAX(CASE WHEN t.name = 'S1' THEN gpt.is_done END) AS 'S1', 
MAX(CASE WHEN t.name = 'POLY' THEN gpt.is_done END) AS 'POLY',
MAX(CASE WHEN t.name = 'ED-RTN' THEN gpt.is_done END) AS 'ED-RTN', 
MAX(CASE WHEN t.name = 'ED-S3' THEN gpt.is_done END) AS 'ED-S3', 
MAX(CASE WHEN t.name = 'ED-S2' THEN gpt.is_done END) AS 'ED-S2' 
FROM geodetic_point_tasks AS gpt
INNER JOIN missions AS m ON gpt.mission_id = m.id
INNER JOIN geodetic_points AS gp ON gpt.geodetic_point_id = gp.id
INNER JOIN tasks AS t ON gpt.task_id = t.id
INNER JOIN geodetic_point_missions AS gpm ON gp.id = gpm.geodetic_point_id
LEFT JOIN users as u ON u.id = gpm.user_id
WHERE m.name = @mission_name AND m.year = @mission_year AND t.type IN ('geodetický bod', 'ochrana GB', 'iné' , 'Geodetické meranie') AND gp.type IN ('ŠNS', 'ŠTS', 'ŠPS-A', 'ŠPS-B', 'ŠPS-C', 'ŠGS')
GROUP BY gp.label;