
-- Martin kalivoda data 
CALL selectOrInsertUser('Martin Kalivoda', 'geodet', NULL, 'Martin', @martin_kalivoda_user_id);

-- Create group SkJanoAndrej and assign users
CALL selectOrInsertGroup('TestovaciaSkupina', @group_test_id);
INSERT INTO `user_groups` (`user_id`, `group_id`) VALUES (@martin_kalivoda_user_id, @group_test_id);

-- Create new mission
CALL selectOrInsertMission('testovacia_misia', '2021', @group_test_id, 0, @last_mission_id);

CALL selectOrInsertGeodeticPoint('Testovací bod 1', 'ŠNS', NULL, @last_geodetic_point_id);
INSERT INTO `geodetic_point_missions` (`mission_id`, `geodetic_point_id`, `group_id`, `status`) VALUES (@last_mission_id, @last_geodetic_point_id, @group_test_id, '0'); 
CALL selectOrInsertTask('Nový bod', 'geodetický bod', @last_task_id);
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Zachovalý', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Zničený', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Nenájdený', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Úprava terénu', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Vyvŕtanie dierky', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Náter GB', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Úprava miestopisu', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Geodetické meranie', 'geodetický bod');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Osadenie novej OT', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Osadenie novej SK', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Náter OT', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Náter SK', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Osadenie OT do správnej polohy', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Osadenie SK do správnej polohy', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Úprava terénu okolo OT alebo SK', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Nálepka', 'ochrana GB');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('Fólia', 'iné');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('SKPOS', 'iné');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);
INSERT INTO `tasks` (`name`, `type`) VALUES  ('SGRN', 'iné');
SET @last_task_id = LAST_INSERT_ID();
INSERT INTO `geodetic_point_tasks` (`mission_id`, `geodetic_point_id`, `task_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`) VALUES (@last_mission_id, @last_geodetic_point_id, @last_task_id, @group_test_id, 0, 0, NULL);