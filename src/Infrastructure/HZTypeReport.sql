SET @mission_name = 'SK-H_IV';
SET @mission_year = 2021;
SELECT gp.id AS 'Id bodu', m.name AS Misia, m.year AS Rok, gp.label AS ČB, gpm.status AS Status, 
gpm.modified AS 'Dátum poslednej úpravy', u.name AS Geodet,
(SELECT group_concat(note, '; ') FROM notes WHERE notes.geodetic_point_id = gp.id) AS Poznámka,
MAX(CASE WHEN t.name = 'Oprava poškodeného HZ' THEN gpt.is_done END) AS 'Oprava poškodeného HZ',
MAX(CASE WHEN t.name = 'Nahradenie poškodeného HZ' THEN gpt.is_done END) AS 'Nahradenie poškodeného HZ',
MAX(CASE WHEN t.name = 'Doplnenie chýbajúceho HZ' THEN gpt.is_done END) AS 'Doplnenie chýbajúceho HZ',
MAX(CASE WHEN t.name = 'Narovnanie mierne nakloneného HZ' THEN gpt.is_done END) AS 'Narovnanie mierne nakloneného HZ',
MAX(CASE WHEN t.name = 'Prestabilizácia HZ' THEN gpt.is_done END) AS 'Prestabilizácia HZ',
MAX(CASE WHEN t.name = 'Premiestnennie HZ na bezpečné miesto' THEN gpt.is_done END) AS 'Premiestnennie HZ na bezpečné miesto',
MAX(CASE WHEN t.name = 'Osadenie nového HZ' THEN gpt.is_done END) AS 'Osadenie nového HZ',
MAX(CASE WHEN t.name = 'Výmena alebo osadenie nového nádstavca' THEN gpt.is_done END) AS 'Výmena alebo osadenie nového nádstavca',
MAX(CASE WHEN t.name = 'Náter a popis HZ' THEN gpt.is_done END) AS 'Náter a popis HZ',
MAX(CASE WHEN t.name = 'Odkopanie hraničného znaku' THEN gpt.is_done END) AS 'Odkopanie hraničného znaku',
MAX(CASE WHEN t.name = 'Dosypanie hraničného znaku' THEN gpt.is_done END) AS 'Dosypanie hraničného znaku',
MAX(CASE WHEN t.name = 'Nahradenie poškodenej PZ' THEN gpt.is_done END) AS 'Nahradenie poškodenej PZ',
MAX(CASE WHEN t.name = 'Nová PZ' THEN gpt.is_done END) AS 'Nová PZ',
MAX(CASE WHEN t.name = 'Nová ochranná tyč' THEN gpt.is_done END) AS 'Nová ochranná tyč' ,
MAX(CASE WHEN t.name = 'Náter ochrannej tyče' THEN gpt.is_done END) AS 'Náter ochrannej tyče' ,
MAX(CASE WHEN t.name = 'Osadenie OT do správnej polohy' THEN gpt.is_done END) AS 'Osadenie OT do správnej polohy',
MAX(CASE WHEN t.name = 'RTN' THEN gpt.is_done END) AS 'RTN',
MAX(CASE WHEN t.name = 'S3' THEN gpt.is_done END) AS 'S3',
MAX(CASE WHEN t.name = 'S2' THEN gpt.is_done END) AS 'S2',
MAX(CASE WHEN t.name = 'S1' THEN gpt.is_done END) AS 'S1',
MAX(CASE WHEN t.name = 'POLY' THEN gpt.is_done END) AS 'POLY',
MAX(CASE WHEN t.name = 'RAJ' THEN gpt.is_done END) AS 'RAJ',
MAX(CASE WHEN t.name = 'ED-RTN' THEN gpt.is_done END) AS 'ED-RTN',
MAX(CASE WHEN t.name = 'ED-S3' THEN gpt.is_done END) AS 'ED-S3',
MAX(CASE WHEN t.name = 'ED-S2' THEN gpt.is_done END) AS 'ED-S2'
FROM geodetic_point_tasks AS gpt
INNER JOIN missions AS m ON gpt.mission_id = m.id
INNER JOIN geodetic_points AS gp ON gpt.geodetic_point_id = gp.id
INNER JOIN tasks AS t ON gpt.task_id = t.id
INNER JOIN geodetic_point_missions AS gpm ON gp.id = gpm.geodetic_point_id
LEFT JOIN users AS u ON gpm.user_id = u.id
WHERE m.name = @mission_name AND m.year = @mission_year AND t.type IN ('hraničný znak', 'ochranná tyč', 'Geodetické meranie') AND gp.type IN ('HZ')
GROUP BY gp.label;