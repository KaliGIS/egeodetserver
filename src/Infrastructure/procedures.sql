-- select or insert user
DELIMITER $$
CREATE OR REPLACE PROCEDURE selectOrInsertUser(IN name TEXT, IN role TEXT, IN email TEXT, IN password TEXT, OUT user_id BIGINT)
BEGIN
    DECLARE userId BIGINT;
    SELECT `id` INTO userId FROM `users` WHERE `users`.`name` = name LIMIT 1; 
    IF (userId IS NULL) THEN 
        INSERT INTO `users` (`name`, `role`, `email`, `password`) VALUES (name, role, email, SHA2(password, 256));
        SET user_id = LAST_INSERT_ID();
    ELSE
        SET user_id = userId;
    END IF;
END $$
DELIMITER ;

-- select or insert task
DELIMITER $$
CREATE OR REPLACE PROCEDURE selectOrInsertTask(IN name TEXT, IN type TEXT, OUT task_id BIGINT)
BEGIN
    DECLARE taskId BIGINT;
    SELECT `id` INTO taskId FROM `tasks` WHERE `tasks`.`name` = name LIMIT 1; 
    IF (taskId IS NULL) THEN 
        INSERT INTO `tasks` (`name`, `type`) VALUES  (name, type);
        SET task_id = LAST_INSERT_ID();
    ELSE
        SET task_id = taskId;
    END IF;
END $$
DELIMITER ;

-- select or insert mission
DELIMITER $$
CREATE OR REPLACE PROCEDURE selectOrInsertMission(IN name TEXT, IN year INT, IN lifecycle_phase INT, OUT mission_id BIGINT)
BEGIN
    DECLARE missionId BIGINT;
    SELECT `id` INTO missionId FROM `missions` WHERE `missions`.`name` = name AND `missions`.`year` = year LIMIT 1; 
    IF (missionId IS NULL) THEN 
        INSERT INTO `missions` (`name`, `year`, `lifecycle_phase`) VALUES  (name, year, lifecycle_phase);
        SET mission_id = LAST_INSERT_ID();
    ELSE
        SET mission_id = missionId;
    END IF;
END $$
DELIMITER ;

-- select or insert group
DELIMITER $$
CREATE OR REPLACE PROCEDURE selectOrInsertGroup(IN name TEXT, OUT group_id BIGINT)
BEGIN
    DECLARE groupId BIGINT;
    SELECT `id` INTO groupId FROM `groups` WHERE `groups`.`name` = name LIMIT 1; 
    IF (groupId IS NULL) THEN 
        INSERT INTO `groups` (`name`) VALUES  (name);
        SET group_id = LAST_INSERT_ID();
    ELSE
        SET group_id = groupId;
    END IF;
END $$
DELIMITER ;

-- select or insert geodetic point
DELIMITER $$
CREATE OR REPLACE PROCEDURE selectOrInsertGeodeticPoint(IN label TEXT, IN type TEXT, IN last_maintenance TIMESTAMP, OUT geodetic_point_id BIGINT)
BEGIN
    DECLARE geodeticPointId BIGINT;
    SELECT `id` INTO geodeticPointId FROM `geodetic_points` WHERE `geodetic_points`.`label` = label AND `geodetic_points`.`type` = type LIMIT 1; 
    IF (geodeticPointId IS NULL) THEN 
        INSERT INTO `geodetic_points` (`label`, `type`, `last_maintenance`) VALUES  (label, type, last_maintenance);
        SET geodetic_point_id = LAST_INSERT_ID();
    ELSE
        SET geodetic_point_id = geodeticPointId;
    END IF;
END $$
DELIMITER ;